function err = costfit(j,r,u,jref)
    jest = [j];
    err = 0;
    % Compute N future values and calculate
    % the least square error
    for i = 2:2
        jest(i) = cigri_model(jest(i-1),r,u);
        
        % Error for going over the reference (A LOT)
        err = err + (jest(i)-jref+1)^2 + (jest(i)>=jref)*1000;
        
    end
end