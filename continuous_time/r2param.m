function out = r2param(r)
    %% Adjust to experimental data
    % Everything was done with 200 initial jobs
    r_list = [4 6 8 10 12]; % Resource number
    t_list = [2500 1800 1350 1200 1100] - 75; % Completion time

    % Now we calculate the rate
    rate = (200-10)./t_list; % jobs/seg

    p = polyfit(r_list, rate, 2); 
    
    if r > 15
        out = polyval(p, 15) + (r-15)*0.001;
    else
        out = polyval(p, r);
    end
end