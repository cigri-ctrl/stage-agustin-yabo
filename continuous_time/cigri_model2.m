function jout = cigri_model(j,r,u)
    
    % Non-negative discrete actuation
    u = u*(u>0);

    % Dynamics
    jout = j - r2param(r) + u;
   
    % Non-negative jobs
    if jout < 0
        jout = 0;
    end
    
end