function jout = cigri_plant(j,r,u)
    
    % Random factor
    fact = 1; % - rand/5;

    % Dynamics
    jout = cigri_model(j,r*fact,u);
    
end