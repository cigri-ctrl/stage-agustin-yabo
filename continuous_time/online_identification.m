%% Simulations w/control

% Lets estimate r with an r0 = 4

close all
clear all

u = 1;
r = 6;
r_hist = r;
rhist = [4];
uhist = [0];
j = [100];
jest = [0];
jref = 60;

for i = 2:1200
    
    % Vary the amount of resources
%     if rand < 0.05
%         r = round(r*(rand + 0.5));
%         r = r - (r<6)*(r-6) - (r>20)*(r-20); % Threshold
%     end
    
    % Job input (closed loop) based on the estimated r
    u = fminsearch(@(u) costfit(j(i-1),rhist(i-1),u,jref),u);
    %u = costfit_discrete(j(i-1),rhist(i-1),jref);
    
    %u = (jref-j(i-1))*0.1;
    u = u*(u>0);

    uhist = [uhist, u];
    j(i) = cigri_plant(j(i-1),r,u);
    
    % Estimation
    jest(i) = cigri_model(j(i-1),rhist(i-1),u);
    
    % Identification of the rate
    err = j(i) - jest(i);
    rhist(i) = rhist(i-1) - 10*err;
    r_hist(i) = r;
    
end

%% Plotting the results

subplot(2,1,1)
hold on
plot(j)

% Now we wanna mark where we violated the job threshold
att = find(j>jref);
att_j = j(att);
plot(1:length(j),60*ones(1,length(j)),'--g')
%scatter(att, j(att),'rx');

title('Jobs in the queue')
subplot(2,1,2)
plot(uhist)
title('Action')

figure(2)
hold on
plot(rhist)
plot(r_hist,'--g')
%plot(rhist,'--r')
title('Unload rate estimation')

