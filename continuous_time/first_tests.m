%% Adjust to experimental data
% Everything was done with 200 initial jobs
r_list = [4 6 8 10 12]; % Resource number
t_list = [2500 1800 1350 1200 1100] - 75; % Completion time

% Now we calculate the rate
rate = (200-10)./t_list; % jobs/seg
p = polyfit(r_list, rate, 2); p_list = polyval(p, r_list);
scatter(r_list, 1./rate);

figure(1); hold on; plot(r_list, 1./t_list); scatter(r_list, 1./t_list); %plot(r_list, 1./p_list)
title('Relation between number of resources and completion time')
xlabel('Resources'); ylabel('Completion time')

%% Simulations varying resources

figure(2)
hold on

for r = 4:2:12
    
    j = [0];
    for i = 2:3000
        % Job input
        u = (i==75)*(200-10); 

        j(i) = cigri_model(j(i-1),r,u);
    end
    plot(j)
    title('Simulation varying resources')
end

%% Simulations varying initial jobs

figure(3)
hold on

for ub = [25, 50, 75, 100, 150, 200]
    j = [0];
    for i = 2:1200
        % Job input
        u = (i==75)*(ub-10); 

        j(i) = cigri_plant(j(i-1),r,u);
    end
    plot(j)
    title('Simulation varying initial jobs')
end

%% Simulations w/control

figure(4)
hold on

j = [100];
for i = 2:1200
    % Job input
    u = (60-j(i-1))*0.1; 

    j(i) = cigri_model(j(i-1),r,u);
end
plot(j)

%% Plot my function
out = [];
for i = 0:20
    out = [out, r2param(i)];
end
figure(5)
plot(0:20, out)