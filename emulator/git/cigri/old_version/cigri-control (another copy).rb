#!/usr/bin/ruby -w

def export2file(type,data,cluster)
  temps = Time.now
  file = File.open("/home/docker/jew/emulator/logs/log.txt", "a+")
  file << "#{type};#{data};#{temps.to_f};#{cluster}\n"
  file.close
  return nil
end

def cigri_model(q,r,u,dt)
    # Non-negative discrete actuation
    u = u*(u>0 ? 1 : 0)
    u = u.to_i

    # Dynamics
    qout = q - 8*r*dt + u
   
    # Non-negative jobs
    if qout < 0
        qout = 0
    end
    
    return qout
end

def costfit_old(q,r,q_ref,dt)
	q_est_final = 0
	u = 0

	while q_est_final < q_ref do
		q_est = [q];

		# Compute the one-step-ahead prediction
		q_est.push(cigri_model(q_est[-1],r,u,dt))
		
		q_est_final = q_est[-1];
		
		u += 1
	end
	
	u = u - 2
	u = u*(u>0 ? 1 : 0)
	
	return u

end

def costfit(q,r,q_ref,dt)
	q_est_final = 0
	u = 0

	while q_est_final < q_ref do
		u += 1
		
		q_est = [q];

		# Compute the one-step-ahead prediction
		q_est.push(cigri_model(q_est[-1],r,u,dt))
		
		q_est_final = q_est[-1];
	end

	u = u - 1
	
	return u

end
