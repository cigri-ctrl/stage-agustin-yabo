# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import requests
import json
import time
import numpy

finished = False
states = []
intJob = "4"

while finished == False:
    finished = True
    
    r = requests.get("http://localhost:46668/cigri-api/campaigns/" + intJob + "/jobs", headers={"X_CIGRI_USER":"docker"})
    data = json.loads(r.content)
    
    ids = []; cls = []; stt = [];
    
    for p in data['items']:
        if p['state'] != 'terminated':
            finished = False
        
        ids.append(p['id'])
        cls.append(p['cluster'])
        stt.append(p['state'])

    states.append(numpy.vstack([ids,cls,stt]))
    
    time.sleep(1)
    
q = []
r = []
t = []

for i in range(0,len(states)):
    q.append(sum(states[i][2]=='pending'))
    r.append(sum(states[i][2]=='running'))
    t.append(i)
    
matplotlib.pyplot.plot(t,q)
matplotlib.pyplot.plot(t,r)