echo "Server?"
read sv
rsync -ave ssh *.sh root@$sv.nancy.g5k:~
rsync -ave ssh *.rb root@$sv.nancy.g5k:~
rsync -ave ssh ../git/cigri/lib/* root@$sv.nancy.g5k:~/git/cigri/lib/
rsync -ave ssh ../git/cigri/modules/* root@$sv.nancy.g5k:~/git/cigri/modules/
rsync -ave ssh ../test_jobs root@$sv.nancy.g5k:~
