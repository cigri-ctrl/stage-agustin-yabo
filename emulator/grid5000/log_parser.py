# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim

finished = False
states = []
intJob = "4"
initial_t = 0
lastrow = -1
reference = 5

strServer = 'griffon-70'

w = []; r = []; f = []; l = []; u = []; u2 = []; xest = []; rest = []; ru = [];
dj = [];
finished = False
# Define some plotting shits
plt.ion()
fig = []; ax = []
for i in [0,1]:
    fig.append(plt.figure()); ax.append(plt.subplot(1,1,1))
    ax[i].set_xlabel('Time [seconds]'); ax[i].set_ylabel('Jobs [#]')
    fig[i].show()

ax[0].plot([],[],color='red'); ax[1].plot([],[],color='red',marker='x',linestyle='--')
ax[0].plot([],[],color='yellow')
ax[0].plot([],[],color='blue')
ax[0].plot([],[],color='yellow',linestyle='--')
ax[1].plot([],[],color='blue')
ax[0].plot([],[],'--',color='grey'); ax[1].plot([],[],color='green')

ax[0].legend(['waiting','waiting est','running','running est', 'reference'])
ax[1].legend(['control input','r_est'])

while finished == False:
    i = 0
    
    output = os.system('scp root@' + strServer + '.nancy.g5k:/root/logs/log.txt .')
    
    # Not always I am able to scp the file
    try:
        with open('log.txt', "r") as csvfile:
            csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
            for row in csvarray:
                
                # Store the initial time to bias the axis
                if i == 0:
                    initial_t = np.double(row[2])
                
                if i > lastrow:
                    if row[0] == 'Waiting':
                        w.append([np.int(row[1]),np.double(row[2]),row[3]])
                    elif row[0] == 'Running':
                        ru.append([np.int(row[1]),np.double(row[2]),row[3]])
                    elif row[0] == 'Action':
                        u.append([np.double(row[1]),np.double(row[2]),row[3]])
                    elif row[0] == 'Submitted':
                        u2.append([np.int(row[1]),np.double(row[2]),row[3]])
                    elif row[0] == 'x_est':
                        xest.append([np.double(row[1]),np.double(row[2]),row[3]])
                    elif row[0] == 'r_est':
                        dj.append([np.double(row[1]),np.double(row[2]),row[3]])
                    elif row[0] == 'p':
                        rest.append([np.double(row[1]),np.double(row[2]),row[3]])
                        
                    lastrow = i
                
                i += 1 # Increment the counter
        
        csvfile.close()
    except: pass

    # Redraw everything        
    tmpw = np.array(w, dtype=object)
    tmpw1 = tmpw[tmpw[:,2]=='tchernobyl']
    tmpw2 = tmpw[tmpw[:,2]=='fukushima']
    tmpw3 = tmpw[tmpw[:,2]=='threemile']
    tmpr = np.array(r, dtype=object)
    tmpdj = np.array(dj, dtype=object)
    tmpru = np.array(ru, dtype=object)
    tmpu = np.array(u, dtype=object)
    tmpu2 = np.array(u2, dtype=object)
    tmpxest = np.array(xest, dtype=object)
    tmprest = np.array(rest, dtype=object)   
    
    try:
        ax[0].lines[4].set_data(tmpw1[:,1]-initial_t,np.ones([1,len(tmpw1)])[0]*reference)
        ax[0].lines[0].set_data(tmpw1[:,1]-initial_t,tmpw1[:,0])
        ax[0].lines[2].set_data(tmpru[:,1]-initial_t,tmpru[:,0])
        ax[0].lines[3].set_data(tmpdj[:,1]-initial_t,tmpdj[:,0])
        ax[0].lines[1].set_data(tmpxest[:,1]-initial_t,tmpxest[:,0])
        
        ax[1].lines[0].set_data(tmpu[:,1]-initial_t,tmpu[:,0])
        ax[1].lines[2].set_data(tmprest[:,1]-initial_t,1/tmprest[:,0])
    except: pass
    
    for i in [0,1]:
        ax[i].relim()
        ax[i].autoscale_view()
        fig[i].canvas.flush_events()
        
    plt.draw()
    time.sleep(1)                