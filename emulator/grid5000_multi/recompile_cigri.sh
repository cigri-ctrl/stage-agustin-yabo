systemctl stop cigri
cd cigri
make install-cigri setup
/etc/init.d/oidentd start
mkdir -p /var/run/cigri
chown -R cigri /var/run/cigri
su - cigri -c "cd /usr/local/share/cigri && /usr/local/share/cigri/modules/almighty.rb"
