%% Simulations w/control

close all
clear all

p = 1/400; alpha = 0.015; kin = 0.07;
p_hist = p;
r = 0; q = 0; f = 0; b = 0;
r_sampled = r; q_sampled = q;
fact_duration = 0;

u = 0; u_hist = 0;

rmax = 60;
rv = [0];

q_est = 0; q_ref = 30; r_ref = 60; f_ref = 4;

dt = 5.;
time = dt:dt:700;

% x = q r p b f kin

H = [1 0 0 0 0 0;0 1 0 0 0 0;0 0 0 0 1 0];

Q = [1   0 0     0 0   0;
     0   1 0     0 0   0;
     0   0 1e-6  0 0   0;
     0   0 0     1 0   0;
     0   0 0     0 0.1 0;
     0   0 0     0 0   0;]*1e2; % Process noise covariance
 
R = 50; % Measurement noise covariance

P = [100 0   0   0   0  0;
     0   100 0   0   0  0;
     0   0   0   0   0  0;
     0   0   0   100 0  0;
     0   0   0   0   10 0;
     0   0   0   0   0  0.1];
 
x_est = [0; 0; 0.01; 0;0;0.1]*[1 1];
x_pred = [0; 0];

for t = time
    i = round((t-dt)/dt + 2);

    % Compute the continuous dynamics of the plant
    [q(i),r(i),f(i)] = cigri_model_fs(q(i-1),r(i-1),f(i-1),p,kin,rmax,u,dt);
    
    % But I only have access to the sampled one
    q_sampled(i) = round(q(i));
    r_sampled(i) = round(r(i));
    
    % I compute my estimation of rmax
    rv(end) = r_sampled(i);
    if length(rv) > 5
        rv = rv(2:end);
    end
    rmax_est = max(rv) + 10;
    
    if i > 100
        rmax = 30;
    end
        
    % Quantized Kalman filter
    ym = [q_sampled(i);r_sampled(i);f(i)] - H*x_est(:,i);
    K = P*H'*inv(R + H*P*H');
    x_est(:,i) = x_est(:,i) + K*ym;
    P = (eye(6) - K*H)*P;

    % Job input (closed loop) based on the estimated x and r
    u = costfit(x_est(1,i),x_est(2,i),f(i),x_est(3,i),x_est(6,i),rmax_est,q_ref,r_ref,f_ref,dt);
    u_hist(i) = u;

    % Prediction
    [x_est(1,i+1),x_est(2,i+1),x_est(5,i+1)] = cigri_model_fs(x_est(1,i),x_est(2,i), ...
                                  x_est(5,i),x_est(3,i),x_est(6,i),rmax_est,u,dt);
    x_est(3,i+1) = x_est(3,i);
    x_est(6,i+1) = x_est(6,i);
    x_est(4,i+1) = y(x_est(1,i+1),x_est(2,i+1),rmax_est);
    x_pred(1,i+1) = x_est(1,i+1);
    x_pred(2,i+1) = x_est(2,i+1);
    A = [1       0                0         -dt  0                   0;
         0 1-dt*x_est(3,i) -dt*x_est(2,i)    dt  0                   0;
         0       0                1          0   0                   0;
         0       0                0          1   0                   0;
         0       0                0          0   1-0.015*dt  x_est(4,i)^2.05;
         0       0                0          0   0                   1];
    P = A*P*A' + Q; 
        
end

time = [0, time];

%% Plotting the results
figure(1)
subplot(3,1,1)
hold on
stairs(time, round(q))
stairs(time, round(r),'green')
plot(time,x_est(2,1:end-1),'red')

legend('waiting jobs','running jobs','estimated r')

% Now we wanna mark where we violated the job threshold
% att = find(q>q_ref);
% scatter(att*dt, q(att),'rx');

title('Jobs in the queue')
subplot(3,1,2)
stairs(time, u_hist)
title('Action')

subplot(3,1,3)
plot(time, f)
title('Fileserver usage')

figure(2)
hold on
plot(time, 1/p*ones(1,length(time)), '--g')
plot(time, 1./x_est(3,1:end-1))
legend('real p', 'estimated p')
title('Unload rate estimation')

figure(3)
hold on
plot(time,kin*ones(1,length(time)), '--g')
plot(time,x_est(6,1:end-1))
legend('real p', 'estimated p')
title('Kin estimation')

