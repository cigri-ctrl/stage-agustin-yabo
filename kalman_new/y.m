function out = y(q,r,rmax)

    out = rmax - r;
    if q < out
        out = q;
    end
    if out < 0
        out = 0;
    end
    
end