function u = costfit(q,r,f,p,kin,rmax,q_ref,r_ref,f_ref,dt)
q_est_final = 0;
r_est_final = 0;
f_est_final = 0;
reached_maximum = 0;
u = 0;

while (r_est_final < r_ref && reached_maximum == 0 && q_est_final < q_ref && f_est_final < f_ref)
        
    u = u + 1;
    
    q_est = [q];
    r_est = [r];
    f_est = [f];

    % Compute N future values and calculate
    % the least square error
    for i = 2:3
        [q_est(i),r_est(i),f_est(i)] = cigri_model_fs(q_est(i-1),r_est(i-1),f_est(i-1),p,kin,rmax,u,dt);
    end

    if r_est(end) == r_est_final
        reached_maximum = 1;
    end
    
    q_est_final = q_est(end);
    r_est_final = r_est(end);
    f_est_final = f_est(end);
    
end

u = u - 1;

end