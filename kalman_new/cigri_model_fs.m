function [qout,rout,fout] = cigri_model(q,r,f,p,kin,rmax,u,dt)
    
    % Non-negative discrete actuation
    u = u*(u>0); u = round(u);
    
    b = y(q,r,rmax);
   
    % Dynamics
    qout = q + u - b;
    rout = r + b - r*p*dt;
    
    % Fs dynamics
    fout = kin*b^2.05 + (1-0.015*dt)*f;
   
    % Non-negative jobs
    if qout < 0
        qout = 0;
    end
    
    if rout < 0
        rout = 0;
    end
    
    if fout < 0
        fout = 0;
    end
    
    if rout > rmax
        rout = rmax;
    end
        
end