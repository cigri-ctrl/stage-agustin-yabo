#!/usr/bin/ruby -w

def export2file(type,data,cluster,file)
  temps = Time.now
  file = File.open(file, "a+")
  file << "#{type};#{data};#{temps.to_f};#{cluster}\n"
  file.close
  return nil
end

def b(q,r,rmax)
    out = rmax - r
    if q < out
        out = q
    end
    return out
end

def cigri_model(q,r,f,p,kin,rmax,u,dt)
    # Non-negative discrete actuation
    u = u*(u>0 ? 1 : 0)
    u = u.to_i

    btmp = b(q,r,rmax)

    # Dynamics
    qout = q - btmp + u
    rout = r + btmp - p*r*dt
    fout = kin*(btmp**2.05) + (1-0.015*dt)*f
   
    # Non-negative jobs
    if qout < 0
        qout = 0
    end
    
    if rout < 0
        rout = 0
    end
    
    if rout > rmax
        rout = rmax
    end
    
    return [qout,rout,fout]
end

def costfit(q,r,f,p,kin,rmax,q_ref,r_ref,dt,overload)
    q_est_final = 0
    r_est_final = 0
    f_est_final = 0
    reached_maximum = false
    u = 0

    #while ((r_est_final < r_ref) and not(reached_maximum) and q_est_final <= q_ref) or ((q_est_final < q_ref) and not(overload)) do
    while (r_est_final < r_ref) and not(reached_maximum) and q_est_final <= q_ref and f_est_final <= f_est do
        u += 1
    
        q_est = [q]
        r_est = [r]
        f_est = [f]
        
        for i in 1..2
            q_est[i],r_est[i],f_est[i] = cigri_model(q_est[-1],r_est[-1],f_est[-1],p,kin,rmax,u,dt)
        end
        
        # Do I have an absolute maximum?
        reached_maximum = (r_est[-1] == r_est_final)

        # Compute the one-step-ahead prediction
        q_est_final = q_est[-1]
        r_est_final = r_est[-1]
        f_est_final = f_est[-1]
        
    end

    u = u - 1
    
    return u

end
