#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 14:32:00 2018

@author: jew
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import random
from scipy import stats

t = np.array([-0.07, -0.11, -0.143, -0.175, -0.2])
a = np.array([4, 6, 8, 10, 12])

slope, intercept, r_value, p_value, std_err = stats.linregress(a, t)

plt.figure(0, figsize=(6, 3))
plt.plot(a, intercept + slope*a, 'r', label='Linear regression', linestyle='--')
plt.scatter(a,t, label='Slope evolution', marker='o')
plt.xlabel('Resources')
plt.ylabel('Slope')
plt.legend()
plt.savefig('test.eps', bbox_inches='tight', format='eps', dpi=1000)

p = [-0.0009,0.0278,-0.0204]

plt.figure(1, figsize=(4, 3))
for r in range(4,14,2):
    
    j = [0];
    x = [0];
    for i in range(2,3000):
        u = (i==75)*(200-2)*(1-r*0.001)
        
        j.append(j[-1] - (r**2*p[0]+r*p[1]+p[2]) + u + 0.3*(random.random()-0.5))
        x.append(i)
        if j[-1] < 0:
            j[-1] = 0
    
    a = []
    for i in range(0,len(j)):
        a.append(1 + random.random()*10)
        
    j = np.floor(np.array(j))[range(0,len(j),15)]
    x = np.round(np.array(x))[range(0,len(x),15)]+random.random()*10
    plt.step(x,j, label=str(r)+' resources')

plt.legend()
plt.xlim([0,2750])
plt.ylim([0,200])
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
plt.grid(linestyle='dotted')
plt.savefig('test2.eps', bbox_inches='tight', format='eps', dpi=1000)

plt.figure(2, figsize=(4, 3))
for r in range(50,250,50):
    
    j = [0];
    x = [0];
    for i in range(2,1200):
        u = (i==75)*(r-2)*(1-2*0.005)
        
        j.append(j[-1] - (12**2*p[0]+12*p[1]+p[2]) + u + 0.3*(random.random()-0.5))
        x.append(i)
        if j[-1] < 0:
            j[-1] = 0
    
    a = []
    for i in range(0,len(j)):
        a.append(1 + random.random()*10)
        
    j = np.floor(np.array(j))[range(0,len(j),15)]
    x = np.round(np.array(x))[range(0,len(x),15)]+random.random()*10
    plt.step(x,j, label=str(r)+' jobs campaign')

plt.legend()
plt.xlim([0,1200])
plt.ylim([0,200])
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
plt.grid(linestyle='dotted')
plt.savefig('test3.eps', bbox_inches='tight', format='eps', dpi=1000)
