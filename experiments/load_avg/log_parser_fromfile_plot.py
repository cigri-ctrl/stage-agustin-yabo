# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import random

def b(q,r,u,rmax):

    bol = random.random()>0.5
    
    out = rmax - r
    if q < out:
        out = q;
    return out
    

def cigri_model(q,r,p,rmax,u,dt):
    
    u = u*(u>0); u = round(u)
    
    btmp = b(q,r,u,rmax)

    qout = q + u - btmp
    rout = r + btmp - r*p*dt
   
    if qout < 0:
        qout = 0
    
    if rout < 0:
        rout = 0
    
    return qout,rout

def tune(q_sim, tmpt, t1, t2, f):
    for j in range(0,len(q_sim)):
        if tmpt[j]>t1 and tmpt[j]<t2:
            q_sim[j] = q_sim[j]*f
    return q_sim

def broadcasting_app(a, L, S ):  # Window len = L, Stride len/stepsize = S
    a = np.array(a)
    nrows = ((a.size-L)//S)+1
    n = a.strides[0]
    return np.lib.stride_tricks.as_strided(a, shape=(nrows,L), strides=(S*n,n))
    

vect = [4,5,6]
modif = {1: 1.18, 4: 0.83, 5: 0.95, 6: 1.19}
modif = {}
selected_for_model = 5
means = []
historic = []
w = []; r = []; u = []; s = []
#r_sim = [0]; q_sim = [0]; p = 1/66; rmax = 13.; dt = 15.; t = [0]
r_sim = [0]; q_sim = [0]; p = 1/60; rmax = 12.; dt = 15.; t = [0]

# Define some plotting shits
plt.ion()

for files in vect:

    initial_t = 0
    lastrow = -1
    reference = 12
    
    i = 0
    
    with open('log'+str(files)+'.txt', "r") as csvfile:
        csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
        
        w.append([])
        r.append([])
        u.append([])
        s.append([])
        
        for row in csvarray:

            # Store the initial time to bias the axis
            if row[0] == 'Action' and initial_t == 0:
                initial_t = np.double(row[2])
            
            if i > lastrow and np.double(row[2])-initial_t < 1200:
                if row[0] == 'Waiting':
                    if files in modif:
                        alpha = modif[files]
                    else:
                        alpha = 1
                        
                    w[vect.index(files)].append([np.int(np.int(row[1])*alpha),np.double(row[2])-initial_t,row[3]])
                elif row[0] == 'Running':
                    r[vect.index(files)].append([np.int(row[1]),np.double(row[2])-initial_t,row[3]])
                elif row[0] == 'Action':
                    u[vect.index(files)].append([np.double(row[1]),np.double(row[2])-initial_t,row[3]])
                elif row[0] == 'Stress':
                    s[vect.index(files)].append([np.double(row[1]),np.double(row[2])-initial_t,row[3]])
                    
                lastrow = i
            
            i += 1 # Increment the counter
    
    csvfile.close()             
    
# Compute the simulation
i = vect.index(selected_for_model)
tmpwi = np.array(w[i], dtype=object)
tmpri = np.array(r[i], dtype=object)
tmpui = np.array(u[i], dtype=object)
t_sim = [0]
    
# Compute the model
for o in np.arange(dt,int(tmpwi[-1,1]),dt):
    t_sim.append(o)
    if (tmpui[:,1]-o)[tmpui[:,1]-o<15][-1] > 0:
        u_sim = (tmpui[:,0])[tmpui[:,1]-o<15][-1]
    else:
        u_sim = 0
    
    qout, rout = cigri_model(q_sim[-1],r_sim[-1],p,rmax,u_sim,dt)
    q_sim.append(qout)
    r_sim.append(rout - (rout>12)*(rout-12))

w_interp = []
r_interp = []
markeri = ['x','o','+','.']

# Graph all curves and compute the simulation    
for i in range(0,len(w)):
    tmpwi = np.array(w[i], dtype=object)
    tmpri = np.array(r[i], dtype=object)
    tmpui = np.array(u[i], dtype=object)
        
    w_interp.append([])
    w_interp[i].append(np.interp(t_sim,
            np.array(tmpwi[:,1], dtype=np.float64),
            np.array(tmpwi[:,0], dtype=np.float64)))
    
    r_interp.append([])
    r_interp[i].append(np.interp(t_sim,
            np.array(tmpri[:,1], dtype=np.float64),
            np.array(tmpri[:,0], dtype=np.float64)))
    
    means.append(np.mean(tmpwi[:,0]))
    
    plt.figure(0, figsize=(8, 3))
    plt.xlabel('Time [seconds]')
    plt.ylabel('Jobs')
#    plt.scatter(tmpwi[:,1],tmpwi[:,0],
#                label='Experiment ' + str(i+1),
#                alpha=0.2,
#                s=20)

    plt.scatter(tmpwi[:,1],tmpwi[:,0],
            label='Experiment ' + str(i+1),
            alpha=0.2,
            s=20,
            marker=markeri[i],
            color='grey')
        
    plt.legend()
    
    plt.figure(1, figsize=(8, 3))
    plt.xlabel('Time [seconds]')
    plt.ylabel('Resources')
    plt.plot(tmpri[:,1],np.ones([1,len(tmpri)])[0]*reference,'--',color='grey')
    plt.scatter(tmpri[:,1],tmpri[:,0], alpha=0.5)
    
    plt.ylim([-1.5,13.5])
    plt.yticks(range(0,15,3))
    
    total_usage = sum(np.diff(tmpri[:,1])*tmpri[1:,0])/(tmpri[tmpri[:,0]>0][-1,1]-tmpri[tmpri[:,0]>0][0,1])
    total_time = (tmpri[tmpri[:,0]>0][-1,1]-tmpri[tmpri[:,0]>0][0,1])

# Compute means
mean_exp = np.array(w_interp).mean(axis=0)[0]
mean_r = np.array(r_interp).mean(axis=0)[0]
dev_exp = np.array(w_interp).std(axis=0)[0]
dev_r = np.array(r_interp).std(axis=0)[0]

# Plot the model
plt.figure(0, figsize=(8, 3))
plt.step(t_sim,q_sim,label='Model output', color='grey', linewidth=1.5)
plt.legend()
plt.savefig('validation_experiments.pdf', bbox_inches='tight', format='pdf', dpi=1000)

plt.figure(1, figsize=(8, 3))
plt.step(t_sim,r_sim)

plt.figure(4, figsize=(8,3))
plt.step(t_sim,r_sim,label="Model output", color='grey', linewidth=1.5)
plt.errorbar(t_sim, mean_r, dev_r, linestyle='None',
             marker='o',label='Experiments averaged',
             alpha=0.5, markersize=5)
#plt.xlim([0,400])


# Plot the final curve
plt.figure(3, figsize=(8, 3))
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
plt.step(t_sim,q_sim,label='Model output', color='grey', linewidth=1.5)
#plt.step(t_sim,mean_exp,label='Experiments averaged', linestyle='--')
plt.errorbar(t_sim, mean_exp, dev_exp, linestyle='None',
             marker='o',label='Experiments averaged',
             alpha=0.5, markersize=5)
plt.legend()
plt.savefig('validation_averaged.pdf', bbox_inches='tight', format='pdf', dpi=1000)