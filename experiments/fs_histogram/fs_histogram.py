# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import datetime
import matplotlib.pyplot as plt
from collections import defaultdict
from scipy import interpolate
from scipy import signal
from scipy import stats

jobs = defaultdict(lambda: [[],[],[],[]]) # Start, Stop, Temp, Vector

def date2unix(date):
    return time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S+01").timetuple())

strLogFolder = '.'

# The time range I am interested in
#time_range = [0,1000000]
#time_range = [0,130000]
time_range = [410000,450000]
#time_range = [575000,615000]

with open('output.csv', "r") as csvfile:
    
    legends = []
    csvarray = csv.reader(csvfile, delimiter=',', quotechar='|')
    i = 0
    plt.figure(0); plt.xlim(time_range)
    
    for row in csvarray:
        # Store the initial time to bias the axis
        if i == 0:
            initial_t = date2unix(row[9])
            i = 1
        
        # Dont look at the hole interval
        if date2unix(row[9])-initial_t > time_range[0] and \
           date2unix(row[9])-initial_t < time_range[1]:
            jobs[row[1]][0].append(date2unix(row[9])-initial_t)
            jobs[row[1]][1].append(date2unix(row[10])-initial_t)
        
    
    for campaign in jobs:  
        job_vect = np.hstack([np.ones(len(jobs[campaign][0])),
                           -np.ones(len(jobs[campaign][1]))])
        
        job_tmp = np.hstack([jobs[campaign][0],jobs[campaign][1]])
        
        zips = sorted(zip(job_tmp,job_vect))
        
        jobs[campaign][2] = [y for y,x in zips]
        jobs[campaign][3] = np.cumsum([x for y,x in zips])
        
        plt.plot(jobs[campaign][2],jobs[campaign][3])
        
        legends.append(campaign)
        
    plt.legend(legends)    
    csvfile.close()

fig,axes = plt.subplots(2,2,sharex=True, sharey=True, figsize=(6, 4))
fig.add_subplot(111, frameon=False)
fig.text(0.5, 0, 'Time [seconds]', ha='center')
fig.text(0.04, 0.5, 'Frequency', va='center', rotation='vertical')
#plt.title('Histogram of different campaigns')
j = 1
for i in ['11256','11273','11285','11282']:
    duration = np.array(jobs[i][1])-np.array(jobs[i][0])
    if i == '11256':
        duration = duration - 700
        duration = duration[duration>0]
    if i == '11273':
        duration = duration[range(0,round(len(duration)*0.8))]
    
    plt.subplot(2,2,j)
    hist = plt.hist(duration, normed=False, bins=50)
    xt = plt.xticks()[0]  
    xmin, xmax = min(xt), max(xt)
    lnspc = np.linspace(xmin, xmax, len(duration))
    xh = [0.5 * (hist[0][r] + hist[1][r+1]) for r in range(len(hist[1])-1)]
    binwidth = 2.7*(max(xh) - min(xh)) / len(hist[1]) 
    m, s = stats.norm.fit(duration) # get mean and standard deviation  
    pdf_g = stats.norm.pdf(lnspc, m, s)*binwidth*len(duration) # now get theoretical values in our interval  
    plt.plot(lnspc, pdf_g, linestyle='--') # plot it
    print(np.std(duration)/np.mean(duration))
    #plt.xlabel('Time [seconds]')
    #plt.ylabel('Jobs')
    j += 1

plt.savefig('test.eps', bbox_inches='tight', format='eps', dpi=1000)



