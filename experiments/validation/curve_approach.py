#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 14:32:00 2018

@author: jew
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import random
from scipy import stats

def y(q,r,rmax):

    out = rmax - r
    if q < out:
        out = q;
    return out
    

def cigri_model(q,r,p,rmax,u,dt):
    
    u = u*(u>0); u = round(u)
    
    ytmp = y(q,r,rmax)

    qout = q + u - ytmp
    rout = r + ytmp - r*p*dt
   
    if qout < 0:
        qout = 0
    
    if rout < 0:
        rout = 0
    
    return qout,rout

input = [3, 1, 1, 5, 1, 1, 4, 1, 1, 8, 1, 1, 9, 1, 1, 6, 1, 7, 1, 1, 8, 1, 1,
         1, 8, 1, 1, 7, 1, 5, 1, 1, 9, 1, 2, 3, 1, 1, 2, 1, 1, 5, 1, 1, 1, 6,
         1, 1, 8, 2, 1, 9, 1, 1, 6, 1, 1, 1, 9, 1, 8, 1, 1, 4, 1, 1, 1, 7, 1,
         1, 8, 3, 1, 1, 7, 1, 1, 5, 2, 1, 5, 1, 9, 1, 6, 1, 1, 8, 1, 1, 9, 1,
         6, 1, 1, 7, 1, 1]

r = [0]; q = [0]; p = 1/30; rmax = 12.; dt = 15.; t = [0]

plt.figure(1, figsize=(4, 3))
for ti in range(1,round(1605/dt)):
    
    if len(input) > 0:
        u = input[0]
        input = input[1:]
    else:
        u = 0
    
    qout, rout = cigri_model(q[-1],r[-1],p,rmax,u,dt)
    q.append(qout)
    r.append(rout)
    t.append(ti)

plt.plot(t,q,label='q')
plt.plot(t,r,label='r')
plt.legend()
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
#plt.savefig('test3.eps', bbox_inches='tight', format='eps', dpi=1000)
