# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim

finished = False
states = []
intJob = "4"
initial_t = 0
lastrow = -1
reference = 12

strServer = 'griffon-7'

w = []; r = []; u = []
finished = False
# Define some plotting shits
plt.ion()
fig = []; ax = []
for i in range(0,3):
    fig.append(plt.figure())
    ax.append(plt.subplot(1,1,1))
    ax[-1].set_xlabel('Time [seconds]')
    fig[-1].show()

ax[0].plot([],[],color='blue'); ax[0].set_ylabel('Jobs')
ax[1].plot([],[],color='blue'); ax[1].set_ylabel('Resources')
ax[1].plot([],[],'--',color='grey')
ax[2].plot([],[],color='red',marker='x',linestyle='--'); ax[2].set_ylabel('Jobs')

ax[0].legend(['Waiting queue'])
ax[1].legend(['Used resources','Maximum resources'])
ax[2].legend(['CiGri job submission'])

while finished == False:
    i = 0
    
    output = os.system('scp root@' + strServer + '.nancy.g5k:/root/logs/log.txt .')
    
    # Not always I am able to scp the file
    try:
        with open('log.txt', "r") as csvfile:
            csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
            for row in csvarray:
                
                # Store the initial time to bias the axis
                if i == 0:
                    initial_t = np.double(row[2])
                
                if i > lastrow:
                    if row[0] == 'All':
                        w.append([np.int(row[1]),np.double(row[2]),row[3]])
                    elif row[0] == 'Running':
                        r.append([np.int(row[1]),np.double(row[2]),row[3]])
                    elif row[0] == 'Action':
                        u.append([np.double(row[1]),np.double(row[2]),row[3]])
                        
                    lastrow = i
                
                i += 1 # Increment the counter
        
        csvfile.close()
    except: pass

    # Redraw everything        
    tmpw = np.array(w, dtype=object)
    tmpr = np.array(r, dtype=object)
    tmpu = np.array(u, dtype=object) 
    
    ax[0].lines[0].set_data(tmpw[:,1]-initial_t,tmpw[:,0])
    ax[1].lines[0].set_data(tmpr[:,1]-initial_t,tmpr[:,0])
    ax[1].lines[1].set_data(tmpr[:,1]-initial_t,np.ones([1,len(tmpr)])[0]*reference)
    ax[2].lines[0].set_data(tmpu[:,1]-initial_t,tmpu[:,0])
    
    for i in range(0,3):
        ax[i].relim()
        ax[i].autoscale_view()
        fig[i].canvas.flush_events()
        
    plt.draw()
    time.sleep(1)              
    
# More plottable style

ax[0].plot([],[],color='blue'); ax[0].set_ylabel('Jobs')
ax[1].plot([],[],color='blue'); ax[1].set_ylabel('Resources')
ax[1].plot([],[],'--',color='grey')
ax[2].plot([],[],color='red',marker='x',linestyle='--'); ax[2].set_ylabel('Jobs')


plt.figure(0, figsize=(6, 4))

plt.subplot(2,1,1)
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
plt.step(tmpw[:,1]-initial_t,tmpw[:,0])
plt.legend(['Waiting queue'])

plt.subplot(2,1,2)
plt.xlabel('Time [seconds]')
plt.ylabel('Resources')
plt.plot(tmpr[:,1]-initial_t,np.ones([1,len(tmpr)])[0]*reference,'--',color='grey')
plt.step(tmpr[:,1]-initial_t,tmpr[:,0])
plt.ylim([-1.5,13.5])
plt.yticks(range(0,15,3))
plt.legend(['Used resources','Maximum resources'])
plt.savefig('test.eps', bbox_inches='tight', format='eps', dpi=1000)

plt.figure(1)
plt.plot(tmpu[:,1]-initial_t,tmpu[:,0])
plt.legend(['CiGri job submission'])