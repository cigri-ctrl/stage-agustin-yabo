#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 16:43:16 2018

@author: jew
"""
import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import random
import scipy.io
mat = scipy.io.loadmat('culo.mat')
import matplotlib
matplotlib.rcParams.update({'font.size': 8})

plt.figure(0, figsize=(5, 2))
time = mat['time'][0]
timel = np.hstack([mat['time'][0],mat['time'][0][-1]+mat['dt'][0]])
plt.xlabel('Time [seconds]')
plt.plot(time,mat['r_hist'][0],'--',color='grey',label='$p$')
plt.plot(timel,mat['r_est_hist'][0],'-',label='$\hat{p}$ (recursive estimation)')
plt.plot([],[]);plt.plot([],[])
plt.plot(timel,mat['x_est'][1],'-',label='$\hat{p}$ (Extended Kalman Filter)')
plt.legend()
plt.xlim([0, 380])
plt.grid(linestyle='dotted')
plt.savefig('estimation_ekf.eps', bbox_inches='tight', format='eps', dpi=1000)

plt.figure(1, figsize=(5, 2))
time = mat['time'][0]
timel = np.hstack([mat['time'][0],mat['time'][0][-1]+mat['dt'][0]])
x_est = mat['x_est'][0]
x_real = np.round(mat['q'][0])
plt.xlabel('Time [seconds]')
plt.plot(time-300,x_real,'-',label='Waiting queue $q_k$')
plt.plot([],[]);plt.plot([],[])
plt.plot(time-300,x_est[0:-1],'-',label='Estimated $\hat{q}_k$')
plt.legend()
plt.xlim([200, 300])
plt.ylim([24,32])
plt.grid(linestyle='dotted')
plt.savefig('estimation_ekf.eps', bbox_inches='tight', format='eps', dpi=1000)