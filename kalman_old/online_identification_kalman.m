%% Simulations w/control

close all
clear all

p = 1/400; sf_k = 0.05; sf_ext = 0.5;
p_hist = p;
r = 0; q = 0; s = sf_ext;
r_sampled = r; q_sampled = q;
rhist = [0.001];
fact_duration = 0;

u = 0; u_hist = 0;

rmax = 12;
rv = [0];
jest = [0];

q_est = 0; q_ref = 10; r_ref = 400;

dt = 5.;
time = dt:dt:700;

B = [1;0;0;0];
H = [1 0 0 0;0 1 0 0];

Q = [1 0  0     0;
     0   1 0     0;
     0   0 1e-6  0;
     0   0 0     1]*1e2; % Process noise covariance
 
R = 5000; % Measurement noise covariance

P = [100  0 0   0;
     0  100 0   0;
     0  0 0   0;
     0  0 0   100];
 
x_est = [0; 0; 0.001; 0]*[1 1];
x_pred = [0; 0];

for t = time
    i = round((t-dt)/dt + 2);
    
    % Vary the amount of resources
    %if rand < 0.005*dt
    %    r = r/(rand + 0.5);
        %r = r - (r<6)*(r-6) - (r>20)*(r-20); % Threshold
    %end
    
    if i==80
        %rmax = 16;
        r_ref = 32;
    end

    % Compute the continuous dynamics of the plant
    [q(i),r(i),s(i)] = cigri_model(q(i-1),r(i-1),p,sf_k,sf_ext,rmax,u,dt);
    [jest(i),culo,roto] = cigri_model(q_sampled(i-1),r_sampled(i-1),rhist(i-1),sf_k,sf_ext,rmax,u,dt);
    
    % But I only have access to the sampled one
    q_sampled(i) = round(q(i));
    r_sampled(i) = round(r(i));
    
    % I compute my estimation of rmax
    rv(end) = r_sampled(i);
    if length(rv) > 5
        rv = rv(2:end);
    end
    rmax_est = max(rv) + 1;
        
    % Quantized Kalman filter
    ym = [q_sampled(i);r_sampled(i)] - H*x_est(:,i);
    K = P*H'*inv(R + H*P*H');
    x_est(:,i) = x_est(:,i) + K*ym;
    P = (eye(4) - K*H)*P;

    % Job input (closed loop) based on the estimated x and r
    u = costfit(x_est(1,i),x_est(2,i),x_est(3,i),sf_k,sf_ext,rmax_est,q_ref,r_ref,dt);
    u_hist(i) = u;

    % Prediction
    [x_est(1,i+1),x_est(2,i+1)] = cigri_model(x_est(1,i),x_est(2,i),x_est(3,i),sf_k,sf_ext,rmax_est,u,dt);
    x_est(3,i+1) = x_est(3,i);
    x_est(4,i+1) = y(x_est(1,i+1),x_est(2,i+1),rmax_est);
    x_pred(1,i+1) = x_est(1,i+1);
    x_pred(2,i+1) = x_est(2,i+1);
    if x_est(1,i) >= rmax_est - x_est(2,i) || 1
    A = [1       0                0         -dt;
         0 1-dt*x_est(3,i) -dt*x_est(2,i)    dt;
         0       0                1          0;
         0       0                0          1];
    else
    A = [0       0                0          0;
         1 1-dt*x_est(3,i) -dt*r_sampled(i)  0;
         0       0                1          0;
         0       0                0          1];
    end
    P = A*P*A' + Q;
    
    % Identification of the rate
    err = q_sampled(i) - jest(i);
    rhist(i) = rhist(i-1) + 1*err;
        
end

time = [0, time];

%% Plotting the results
figure(1)
subplot(3,1,1)
hold on
stairs(time, round(q))
stairs(time, round(r),'green')
plot(time, x_pred(1,2:end),'r');
plot(time, x_pred(2,2:end),'r--');

legend('waiting jobs','running jobs','kalman q','kalman r')

% Now we wanna mark where we violated the job threshold
% att = find(q>q_ref);
% scatter(att*dt, q(att),'rx');

title('Jobs in the queue')
subplot(3,1,2)
stairs(time, u_hist)
title('Action')

subplot(3,1,3)
stairs(time, s)

figure(2)
hold on
plot(time, 1/p*ones(1,length(time)), '--g')
plot(time, 1./x_est(3,1:end-1))
plot(time, 1./rhist)
legend('real p', 'estimated p')
title('Unload rate estimation')

