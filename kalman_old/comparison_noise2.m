%% Simulations w/control

close all
clear all

N = 1;
r = 0.02;
r_hist = [r];
r_est_hist = [0.001 0.001]; % We start with a wrong value

u = 0;
u_hist = [0];

errcum = 0;
err_hist = [0];

q0 = 0;
q = [q0];
q_sampled = [q0];
q_est = [0];
q_ref = 60;

dt = 0.1;
time = dt:dt:600;

B = [1;0];
H = [1 0];

Q = [1 0;
     0 1e-5]; % Process noise covariance
 
R = 5000000; % Measurement noise covariance

P = [10  0;
     0  0.5];
 
x_est = [0; 0.001]*[1 1];
x_pred = [0; 0];

for t = time
    i = round((t-dt)/dt + 2);
    
    % Job input (closed loop) based on the estimated r
    %u = costfit(q_sampled(i-1),r_est_hist(i),q_ref,dt);
    u = 0.05 - 0.1*mod(round(t/10),2);
    u_hist(i) = u;
    
    % Real system evolution
    fact = 1;
%     if rand < 0.1
%         fact = 1 + rand/5 - 0.1; % Random factor
%     end
    
    % Compute the continuous dynamics of the plant
    q(i) = cigri_model2(q(i-1),r*fact,u,dt);
    q_sampled(i) = round(q(i));
    
    % But I only have access to the sampled one
    q_sampled(i) = round(q(i));
    
    % My estimation of q based on my estimated r
    q_est(i) = cigri_model2(q_sampled(i-1),r_est_hist(i),u,dt);
    
        % Quantized Kalman filter
    ym = q_sampled(i) - H*x_est(:,i);
    K = P*H'*inv(R + H*P*H');
    x_est(:,i) = x_est(:,i) + K*ym;
    P = (eye(2) - K*H)*P;

    % Prediction
    x_est(1,i+1) = cigri_model2(x_est(1,i),x_est(2,i),u,dt);
    x_est(2,i+1) = x_est(2,i);
    A = [1  -1;
         0   1];
    P = A*P*A' + Q;
    
    % Identification of the rate
    err = (q_sampled(i) - q_est(i));

    r_est_hist(i+1) = r_est_hist(i) - 0.003*err;
    r_hist(i) = r*fact;
    
end

time = [0, time];

%% Plotting the results

subplot(2,1,1)
hold on
stairs(time, round(q))
plot(time,x_est(1,1:end-1))
xlim([500,520])

title('Jobs in the queue')
subplot(2,1,2)
stairs(time, u_hist)
ylim([-0.1 1.1])
title('Action')

figure(2)
hold on
plot(time, r_hist, '--g')
plot([time time(end)+dt], r_est_hist)
plot([time time(end)+dt], x_est(2,:), 'r')
legend('real r', 'naif r', 'EKF r')
title('Unload rate estimation')

figure(3)
hold on
plot(time, err_hist)
title('Evolution of the estimation error')

