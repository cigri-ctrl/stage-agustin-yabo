function jout = cigri_model(j,r,u,dt)
    
    % Non-negative discrete actuation
    u = u*(u>0);

    % Dynamics
    jout = j - r + u;
   
    % Non-negative jobs
    if jout < 0
        jout = 0;
    end
    
end