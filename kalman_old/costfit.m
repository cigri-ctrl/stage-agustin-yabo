function u = costfit(q,r,p,sf_k,sf_ext,rmax,q_ref,r_ref,dt)
q_est_final = 0;
r_est_final = 0;
reached_maximum = 0;
u = 0;

while (r_est_final < r_ref && reached_maximum == 0 && q_est_final < q_ref)
        
    u = u + 1;
    
    q_est = [q];
    r_est = [r];
    s_est = [0];

    % Compute N future values and calculate
    % the least square error
    for i = 2:3
        [q_est(i),r_est(i),s_est(i)] = cigri_model(q_est(i-1),r_est(i-1),p,sf_k,sf_ext,rmax,u,dt);
    end

    if r_est(end) == r_est_final
        reached_maximum = 1;
    end
    
    q_est_final = q_est(end);
    r_est_final = r_est(end);
    s_est_final = s_est(end);
    
end

u = u - 1;

end