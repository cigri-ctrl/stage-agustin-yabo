function [qout,rout,sout] = cigri_model(q,r,p,k,sf,rmax,u,dt)
    
    % Non-negative discrete actuation
    u = u*(u>0); u = round(u);
    
    ytmp = y(q,r,rmax);

    % Dynamics
    qout = q + u - ytmp;
    rout = r + ytmp - r*p*dt;
    sout = r*k + sf;
   
    % Non-negative jobs
    if qout < 0
        qout = 0;
    end
    
    if rout < 0
        rout = 0;
    end
    
end