%% Simulations w/control

close all
clear all

N = 1;
r = 0.02;
r_hist = [r];
r_est_hist = [1 1]; % We start with a wrong value

u = 0;
u_hist = [0];

errcum = 0;
err_hist = [0];

q0 = 0;
q = [q0];
q_sampled = [q0];
q_est = [0];
q_ref = 60;

dt = 0.1;
time = dt:dt:600;

for t = time
    i = round((t-dt)/dt + 2);
    
    % Vary the amount of resources
%     if rand < 0.005
%         r = round(r*(rand + 0.5));
%         r = r - (r<6)*(r-6) - (r>20)*(r-20); % Threshold
%     end
    
    % Job input (closed loop) based on the estimated r
    %u = costfit(q_sampled(i-1),r_est_hist(i),q_ref,dt);
    u = 1;
    u_hist(i) = u;
    
    % Real system evolution
    fact = 1;
%     if rand < 0.1
%         fact = 1 + rand/5 - 0.1; % Random factor
%     end
    
    % Compute the continuous dynamics of the plant
    q(i) = cigri_model2(q(i-1),r*fact,u,dt);
    
    % But I only have access to the sampled one
    q_sampled(i) = round(q(i));
    
    % My estimation of q based on my estimated r
    q_est(i) = cigri_model2(q_sampled(i-1),r_est_hist(i),u,dt);
    
    % Identification of the rate
    err = (q_sampled(i) - q_est(i));

    r_est_hist(i+1) = r_est_hist(i) - 1*err;
    r_hist(i) = r*fact;
    
end

time = [0, time];

%% Plotting the results

subplot(2,1,1)
hold on
stairs(time, round(q))

% Now we wanna mark where we violated the job threshold
att = find(q>q_ref);
scatter(att, q(att),'rx');

title('Jobs in the queue')
subplot(2,1,2)
stairs(time, u_hist)
ylim([-0.1 1.1])
title('Action')

figure(2)
hold on
plot(time, r_hist, '--g')
plot([time time(end)+dt], r_est_hist)
legend('real r', 'estimated r')
title('Unload rate estimation')

figure(3)
hold on
plot(time, err_hist)
title('Evolution of the estimation error')

