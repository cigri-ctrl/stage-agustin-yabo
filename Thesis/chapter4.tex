\chapter{Modeling and control}

\section{Queue modeling}

\subsection{Previous work}
In previous works \cite{stahl2018}, we have tackled the problem of controlling CiGri's waiting queue length on each cluster through a very simple proportional feedback law. Ensuring a fixed number of waiting jobs proved to be an efficient way to maximize the usage of a cluster's idle resources, which was the ultimate objective in the first place. Different open-loop experiments were performed, that showed a clear relationship between the curve's slope and the amount of used resources (Figure \ref{comp_pentea}).

\begin{figure*}[h!]
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/test2}
        \caption{}
        \label{comp_pentea}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/test3}
        \caption{}
    \end{subfigure}
    \caption{Time evolution of the number of jobs in the queue when performing open-loop experiments with sample jobs of 30 seconds of average duration. The experiments consisted of: a) campaigns of 200 jobs, when varying the number of available resources in the cluster; b) campaigns of varying number of jobs using a fixed number of available resources.}
    \label{comp_pente}
\end{figure*}

Ultimately, the system model was identified based on these experiments, from which we obtained a second order continuous-time linear model. However, the approach was not robust to variations in workloads. This is basically because the \emph{spilling} rate of the waiting queue depends both on the number of resources and on the duration of each job, parameters that were both fixed during the identification process. To our best understanding, an accurate dynamical description of the system should comprehend the \emph{spilling} rate as a varying parameter, such that the model can be adapted to fit all possible workflows, as well as variations in the number of available resources.

In addition, and as stated before, the previous model describes the dynamics of the cluster's waiting queue with the job submission mechanism as the input. This dynamical description allows to maximize cluster utilization, but does not contemplate other recurrent problems (described in previous chapter). In this work, incorporating the amount of running jobs in the model allows to control this new measure so that, ultimately, two objectives can be accomplished: 1) maximize idle resources usage; 2) tackle overload problems in the infrastructure towards an improved autonomic management strategy.

\subsection{Components involved}

\subsubsection{Overview}

The idea is to describe the behavior of a single cluster managed by an OAR scheduler. The proposed model comprehends the dedicated waiting queue for \emph{best-effort} jobs in the scheduler (green queue in Figure \ref{modeled}) of length $q$,  considering the jobs sent by the runner as the input $u$. The number of jobs taken from the waiting queue and allocated into resources by the scheduler is denoted as $b$ (for buffer), whereas the number of \emph{best-effort} running jobs in the cluster is represented by $r$. 

\begin{figure}[h!]
 \centering
 \includegraphics[scale=1.8]{images/cigri_oar_irods_loop}
 \caption{Example of system dynamics.}
 \label{modeled}
\end{figure}

\begin{table}[!h]
\centering
\label{my-label}
\begin{tabular}{|c|l|c|}
\hline
$q_k$ & Jobs in the waiting queue at time $k$ & jobs \\
$r_k$ & Jobs running in the cluster at time $k$ & jobs \\
$u_k$ & Jobs sent by the \emph{Runner} to the waiting queue at time $k$ & jobs \\
$b_k$ & Jobs allocated by the OAR scheduler at time $k$ & jobs \\
$r_{max}$ & Maximum number of running jobs allowed & jobs \\
$p$ & Job processing rate & jobs/s \\
\hline
\end{tabular}
\caption{Relevant system variables, description and unit.}
\end{table}

\subsubsection{Maximum number of jobs allowed $r_{max}$}

The scheduler can allocate a maximum number of jobs into the cluster depending on several factors. The most obvious one is, of course, the amount of idle resources in the cluster, but other side factors are also involved. As an example, the user is able to declare specific computing requirements when launching a CiGri campaign, which is taken into account by the scheduler. As a consequence, and since resources in a cluster are not homogeneous, there are cases where idle resources in a cluster do not "fit" the requirements of a CiGri campaign (e.g. if octa-core processors are required, but all idle resources have dual-core processors, the maximum number of allocable jobs at that time is $0$). Therefore, computing the maximum number of jobs allowed in a cluster is not straightforward: we denote this number as $r_{max}$, a parameter that varies over time depending -among others- on resources availability. Then, the impact of the scheduling of local users' jobs (red queue in Figure \ref{modeled}) as well as the running users' jobs (not managed by CiGri) is represented in the model through the variations of this parameter.

\subsubsection{Job allocation's buffer $b$}
The allocation process is entirely managed by the OAR scheduler. As explained before, the scheduler has its own criteria to allocate jobs in the cluster, which is detached from CiGri's server. For this work, practical details of its functioning are out of the scope of the control problem and, therefore, not contemplated by the model. However, based on our observations of the system, we propose a deterministic expression for the buffer $b$ that provides a quite accurate description of the scheduler's behavior, based on the following key constraints:  
\begin{itemize}
    \item $0 \leq r_k \leq r_{max}$ (i.e. it is not possible to use more resources than those available in the cluster).
    \item $0 \leq b_k \leq q_k$ (i.e. it is not possible to allocate more jobs that those available in the waiting queue).
\end{itemize}

\subsubsection{Spilling rate}
The spilling rate is defined as the number of finishing jobs in the cluster over a period of time $\Delta t$, named after its physical analogous of queue systems: the water tank. In this context, the spilling represents the outflow of the cluster. In previous experiments \cite{stahl2018}, we could verify a linear relationship between the outflow of the waiting queue and the amount of running jobs. Based on this results, the spilling rate can be expressed as $p \times r$, where $p$ corresponds to a single job's processing rate.

Although there are several factors behind the processing rate (e.g. the time it takes the scheduler to allocate jobs into resources) it depends mainly on job's execution time. As studied before, execution times in scientific workflows have minor variability among jobs. For this reason, we based our approach on the assumption that the average of the processing rate $p$ remains constant throughout the management of each campaign. 

\clearpage

\subsection{Fluid modeling}

In this system, as in most queuing models in computing systems, quantities are measured in amount of jobs, and so it is only natural to represent the states by integer non-negative variables. However, this approach fails to reproduce the infinitesimal variations of the state variables. As a practical example: let's suppose the cluster is running a single job that has a known computation time of $30$ seconds. This situation is represented by the cluster state $r(j)=1, \, \forall j \in [0,30]$. However, based on our prior knowledge of the job's duration, we could assume that the job will have a $50\%$ of progress at $t=15s$ that can be represented as $r(15)=0.5$\footnote{This interpretation is just an example of what infinitesimal variations in the state can represent in the real system. However, it is not related to the definition of job's \emph{progress} from the Computer Science point of view.}. This interpretation of the problem is what motivates the use of fluid modeling, where integer states are represented by real-valued variables \cite{malrait2011experience}. %This approach has a two-fold advantage: it allows to incorporate an estimation of job's progress in the model as non-integer variations of the queue length, but it also allows to describe the evolution of the system over time using differential equations \cite{malrait2011experience}. 

\subsubsection{Dynamical model}

Based on queuing theory, the dynamics can be described by the following equations,
\begin{align}
    \left\{
    \begin{array}{l}
    q_{k+\Delta t} = q_k + u_k - b_k \\
    r_{k+\Delta t} = r_k + b_k - r_k p \Delta t
    \end{array}
    \right.
\end{align}

%\begin{align}
%    \left\{
%    \begin{array}{l}
%    \dot{q}(t) = u(t) - b(t) \\
%    \dot{r}(t) = b(t) - r(t)p
%    \end{array}
%    \right.
%\end{align}

The model is based on the interpretation that the amount of running jobs $r$ behaves also as a queue, where its inflow is the outflow of the waiting queue $q$. The buffer function $b_k$ is defined as,
\begin{align}
    b_k = \begin{cases}
    k (r_{max} - r_k) \Delta t & \mbox{if } q_k \geq k(r_{max}-r_k) \Delta t \\
    q_k & \mbox{if } q_k < k(r_{max}-r_k) \Delta t
    \end{cases} \label{buffer}
\end{align}

The behavior of the scheduler is modeled as a constrained proportional feedback loop that regulates the amount of running jobs $r_k$ to an $r_{max}$ reference value. The gain $k$ depends on the scheduling time: if the OAR server is overloaded, the scheduler will take more time to allocate the jobs into the cluster, which translates into a lower value of $k$. By contrast, fast scheduling is modeled as a faster controller in $b_k$, meaning a higher value of the gain $k$.

\subsubsection{Maximum cluster usage}
\label{maximum_usage_section}
The scheduler does not allocate jobs immediately after a resource becomes idle. The process involves scanning for idle resources, verifying that resources' specifications matches jobs requirements, and finally allocating the jobs. In practice, this takes non-negligible time, during which the cluster has unexploited idle resources. For this reason, even under the best management strategy, the overall cluster utilization during a campaign cannot be $100\%$.

It is possible to obtain an expression for the real maximum cluster usage by computing the equilibrium points of the system, and thus obtaining the steady state value $\bar{r}$. Enforcing $q_{k+\Delta t} = q_k$ and $r_{k+\Delta t} = r_k$ yields,
\begin{align}
	\bar u = \bar b = \bar r \, p \Delta t. \nonumber
\end{align}
From the expression in \eqref{buffer}, it can be seen that the maximum flow between queues $b_k$ occurs when $q_k>k(r_{max}-r_k) \Delta t$, which means that there is always enough jobs in the waiting queue to satisfy the demand of the scheduler. Under this condition, 
\begin{align}
	\bar{b} &= \bar{r}\, p \Delta t, \nonumber \\
	k(r_{max} - \bar r) \Delta t &= \bar r \, p \Delta t, \nonumber \\
	k\,r_{max} &= \bar r + \bar r \, p, \nonumber \\
	\bar r &= r_{max} \left(\frac{k}{k+p}\right), \nonumber \\
	\bar r &= \frac{r_{max}}{(1 + \frac{p}{k})}. \label{maximum_usage} 
\end{align}

%\begin{align}
%	b(t) &= r(t)p, \nonumber \\
%	k\big(r_{max} - r(t)\big) &= r(t)p, \nonumber \\
%	k\,r_{max} &= r(t)k + r(t)p, \nonumber \\
%	r(t) &= r_{max} \left(\frac{k}{k+p}\right), \nonumber \\
%	r(t) &= \frac{r_{max}}{(1 + \frac{p}{k})}. \label{maximum_usage} 
%\end{align}

From this result, and knowing that $\frac{p}{k} > 0$, we can observe that lower values of the processing rate $p$ yields higher values of $\bar r$. This is coherent with the fact that a very high processing rate means more job allocations per unit of time, which implies more resources being temporarily idle as an undesired effect of the scheduling process. In any case, the processing rate is an inherent property of the type of workload, and thus cannot be modified.

Nevertheless, a higher value of $k$ can compensate this effect. As seen before, this parameter reflects how fast the scheduler reacts to idle resources, and depends mainly on the load of the OAR Server. Then, the cluster usage can be improved by ensuring that the scheduler is loaded as minimum as possible throughout the execution, and since the load of the scheduler depends directly of the amount of jobs in the waiting queue, this would mean keeping the waiting queue as empty as possible.

Before this analysis, it seemed natural to think that maximizing the usage of $\bar r$ can be achieved by maximizing the input $\bar u$. However, results showed there is a trade-off in this regard: both low levels and high levels of $\bar q$ yield under-utilization of the cluster.

\subsubsection{Simulation model}
During experimentation, we could verify that, when working in normal conditions (i.e. the OAR server not being \emph{overloaded}) and fixing $\Delta t=5\,s$, the scheduler is able to allocate all jobs in one cycle. Under this conditions, $b_k$ can be rewritten as,
\begin{align}
    b_k = \begin{cases}
    r_{max} - r_k & \mbox{if } q_k \geq r_{max}-r_k \\
    q_k & \mbox{if } q_k < r_{max}-r_k
    \end{cases} \label{eq:buffer2}
\end{align}

\subsection{Model validation}
\label{section_validation}

The proposed model was subsequently validated by comparing its response to the response of the real system, using in both cases a randomly generated input. The testing environment was composed of a CiGri server, an OAR scheduler and a cluster of 12 resources. Results of each experiment are shown in Figures \ref{all_experiments} and \ref{average_experiments}. In these figures, the accuracy of the model is compared when analyzing the waiting queue length $q_k$. The variability among samples is explained by the fact that the OAR scheduler does not behave in a deterministic way as represented in \eqref{buffer}, since several factors are changing between experiments.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\columnwidth]{images/validation_experiments}
\end{center}
\caption{10 identical experiments were carried out. As an example, only 4 are shown in this Figure, and compared to the model output.}
\label{all_experiments}
\end{figure} 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\columnwidth]{images/validation_averaged}
\end{center}
\caption{The mean and standard deviation values are computed for all the experiments, and compared to the model output.}
\label{average_experiments}
\end{figure}

\clearpage

\section{Filesystem analysis}

There is no one "correct" method for designing scientific workflows: how a job accesses -for reading and writing- disk files in the fileserver is entirely determined by the algorithm designer. Thus, the way CiGri campaigns impact on the fileserver load is unknown in advance. Previous approaches have studied this effect for the set of most frequent campaigns affecting their infrastructure based on historic data \cite{ghazzawi2012mpc}, and elaborated \emph{ad-hoc} solutions to the problem. Towards an autonomic management framework for grid computing systems, we identified certain trends on how scientific workflows usually consume storage resources, and proposed automated solutions to avoid overloading them.

\subsection{Considered scenario}

To simplify the analysis, we will focus on a specific kind of best-effort job that can be broken down to 3 stages:
\begin{description} 
	\item[Reading] the job reads input data from iRods (iget).
	\item[Processing] the job performs certain computations over the input data.
	\item[Writing] the job writes the result data into iRods (iput).
\end{description}

To our knowledge, this structure represents the majority of the scientific workflows currently being processed by CiGri. While most of the time is generally spent on the processing stage (2nd step), the fileserver overloading clearly comes from the reading and writing stages.

%\subsubsection{Sample disk reading/writing}

\subsubsection{Measuring disk I/O}

For obtaining metrics of the server's load, we use the $/proc/loadavg$ file, available in every Linux operating system, often used for measuring CPU load and disk I/O. This file provides an exponential moving average of the load computed over a period of 1 minute, where most recent values are weighted higher than the old ones \cite{gunther2011linux}.

\subsection{Dynamical model}

We propose to model the fileserver's load as a subsystem state $f_k \geq 0$, with two inputs $u^1_{k}$ and $u^2_{k}$ representing the contributions from the \emph{reading} and \emph{writing} steps of each job respectively. Preliminary experiments (Figure \ref{open_loop}) showed that the associated homogeneous dynamic representation of the system can be approximated by a simple first-order linear equation,
\begin{align}
	f_{k+\Delta t} = (1 - \alpha \Delta t) f_k + u^1_{k} \Delta t + u^2_{k} \Delta t. \label{fsgeneral}
\end{align}

\begin{figure}[h!]
 \centering
 \includegraphics[scale=1]{images/open_loop}
 \caption{Open-loop experiment performed to analyze the homogeneous response of the system.}
 \label{open_loop}
\end{figure}

The parameter $\alpha$ describes the \emph{unloading} speed of the fileserver, which depends mainly on the inherent averaging of the \emph{loadavg} (load average) sensor, but also on several caching processes behind disk I/O that keeps the CPU in use even after the storage operation is apparently\footnote{By apparently, we refer to the user's point of view: caching processes are used, among other reasons, for making I/O operations transparent to the user, even when they still continue in background.} finished. In this work, we focused on the impact of the \emph{reading} step onto the fileserver. This implies $u^2_k=0, \, \forall k>0$.

\subsubsection{Data reading}

The impact on the fileserver due to \emph{reading} operations can be expressed in terms of the number of allocations $b$, as it reflects the amount of starting jobs at each time instant. %Similarly, the amount of finishing jobs determines the load from the \emph{writing} stage, and so it depends on the spilling rate $p \times r$.
The impact of $b$ is characterized by a non-linear influence of the amount of starting jobs onto the fileserver's load, and a certain delay from the moment the job is allocated until the job starts using the storage.
Under the above-mentioned, the input $u^1_k$ can be described by the general expression,
\begin{align}
	u^1_k = k_{in} (b_{k-i})^\beta, \nonumber
\end{align}
which characterizes the type of loading by a gain $k_{in}$, a delay $i$ and an exponent $\beta$. Then, the expression \eqref{fsgeneral} when $u^2_k=0$, can be written as,
\begin{align}
	f_{k+\Delta t} = (1 - \alpha \Delta t) f_k + k_{in} (b_{k-i})^\beta \Delta t. \nonumber
\end{align}  % + k_{out} r_k p \Delta t.

\begin{table}[!h]
\centering
\label{my-label}
\begin{tabular}{|c|l|c|}
\hline
$f_k$ & Fileserver load average at time $k$ \\
$\alpha$ & Unloading rate of the fileserver \\
$k_{in}$ & Gain related to workflow I/O \\
$i$ & Time delay \\
\hline
\end{tabular}
\caption{Relevant system variables, description and unit.}
\end{table}

After several experiments, we could verify that certain parameters remain essentially constant for all analyzed scenarios. As an example, Figure \ref{fs_filesizes} shows 3 experiments in which we varied the file size used by the job. Throughout the experiments, the values of $\beta$, $\alpha$ and $i$ remained constant, while the gain $k_{in}$ varied considerably.

\begin{figure}[h!]
 \centering
 \includegraphics[scale=1]{images/fs_filesizes}
 \caption{Different experiments targeted to understand how the size of the file used by jobs affects the load of the fileserver.}
 \label{fs_filesizes}
\end{figure}

To generalize this idea, we performed experiments by varying file size, hardware of the fileserver, and job average duration. Later on, we implemented a simple non-linear least squares algorithm for offline identification of the parameters in the model. From this experiment, we concluded that $\beta \approx 2.05$, and $i=2$ when fixing the cycle step $\Delta t = 5s$. The parameter $\alpha$ is linked to the hardware of the fileserver: higher specifications implies better performance, which implies a faster homogeneous response, while poorer hardware takes longer to "recover" from a high load situation. However, through our experiments, there weren't significant variations on this parameter. Based on this observation, we fixed the value $\alpha = 0.015$.

On the other hand, parameter $k_{in}$ heavily relies on the amount of files used by the job, and their size, and so it varies for every different workflow. For this reason, a robust approach requires this parameter to be identified online for every new workflow.

\subsection{Model validation}

Following the same methodology applied in \ref{section_validation}, we validated the proposed model against experiments not used in the modeling steps. In these cases, the cluster was composed of $60$ resources. Results are shown in Figures \ref{valid1} and \ref{valid2}.

\begin{figure}[h!]
 \centering
 \includegraphics[scale=1]{images/loadavg_experiments}
 \caption{3 different results for the same experiment. Starting jobs $b$ are computed from the increments of the running jobs curve $\Delta r_k = r_k-r_{k-1}$.}
 \label{valid1}
\end{figure}

\begin{figure}[h!]
 \centering
 \includegraphics[scale=1]{images/loadavg_model}
 \caption{The mean and standard deviation values are computed for all the experiments, and compared to the model output.}
 \label{valid2}
\end{figure}

\subsection{Multi-storage infrastructure}
In real infrastructures, it is logical to consider more than one fileserver, and jobs that can either impact on one of them but also on several servers. Again, our approach is based on the fact that this is not know a priori, yet it can be inferred during execution.
Let us assume a single cluster infrastructure that has $n$ fileservers with loads $f^1,\hdots,f^n$,
\begin{align}
    \left\{
    \begin{array}{l}
    q_{k+\Delta t} = q_k + u_k - b_k \\
    r_{k+\Delta t} = r_k + b_k - r_k p \Delta t \\
    f^1_{k+\Delta t} = (1 - \alpha \Delta t) f^1_k + k_{in}^1 (b_{k-i})^\beta \Delta t \\
    \qquad \quad \vdots \\
    f^n_{k+\Delta t} = (1 - \alpha \Delta t) f^n_k + k_{in}^n (b_{k-i})^\beta \Delta t
    \end{array}
    \right. \label{multi-server}
\end{align}

For this system, the way each workflow impacts on the fileservers will be determined by the set of parameters $k^1_{in}, \hdots, k^n_{in}$. As an example, an infrastructure composed of 3 server where the I/O operations affect only one of them, could be represented by the following gains,
\begin{align}
	(k^1_{in}, k^2_{in}, k^3_{in}) = (5,0,0) \nonumber
\end{align}

Coping with several fileservers presents no further difficulties than the case of a single fileserver, as it is part of the parameter estimation scheme.

\section{Parameter estimation}

The estimation problem can be formulated by extending the current state space in \eqref{multi-server} to incorporate the parameters to be estimated \cite{wan2000unscented},
\begin{align}
    \left\{
    \begin{array}{l}
    q_{k+\Delta t} = q_k + u_k - b_k \\
    r_{k+\Delta t} = r_k + b_k - r_k p_k \Delta t \\
    f^1_{k+\Delta t} = (1 - \alpha \Delta t) f^1_k + k_{in}^1 (b_{k-i})^\beta \Delta t + w^1_k\\
    \qquad \quad \vdots \\
    f^n_{k+\Delta t} = (1 - \alpha \Delta t) f^n_k + k_{in}^n (b_{k-i})^\beta \Delta t + w^n_k \\
    p_{k+\Delta t} = p_k + w^{n+1}_k \\
    k^1_{in,k + \Delta t} = k^1_{in,k} + w^{n+2}_k \\
	\qquad \qquad \vdots \\    
    k^n_{in,k + \Delta t} = k^n_{in,k} + w^{2n+1}_k
    \end{array}
    \right. \nonumber
\end{align}
where $b_k$ is computed as the buffer function introduced in the expression \eqref{eq:buffer2}, and $w^1_k,\hdots,w^n_k$ are the process noises associated to the impact of external agents on the fileservers. This latter due to the fact that, in the analyzed infrastructure, fileservers are not dedicated to the CiGri environment, but shared among users that can also consume storage resources. As most of the parameters in the model can be time-varying, it is necessary to include this behavior as process noise in the estimation scheme, represented by $w^{n+1}_k, \hdots, w^{2n+1}_k$. For simplification, the experimental phase of this work was performed with two fileservers,
\begin{align}
    \left\{
    \begin{array}{l}
    q_{k+\Delta t} = q_k + u_k - b_k \\
    r_{k+\Delta t} = r_k + b_k - r_k p_k \Delta t \\
    f^1_{k+\Delta t} = (1 - \alpha \Delta t) f^1_k + k^1_{in} (b_{k-i})^\beta \Delta t + w^1_k\\
    f^2_{k+\Delta t} = (1 - \alpha \Delta t) f^2_k + k^2_{in} (b_{k-i})^\beta \Delta t + w^2_k\\
    p_{k+\Delta t} = p_k + w^{3}_k \\
    k^1_{in,k + \Delta t} = k^1_{in,k} + w^{4}_k \\
    k^2_{in,k + \Delta t} = k^2_{in,k} + w^{5}_k
    \end{array}
    \right. \nonumber
\end{align}

The method used in this work for the estimation process is the well-known EKF (Extended Kalman Filter), for being able to deal with the non-linearities of the extended state-space representation. While the accuracy of the EKF has been challenged in the past\footnote{The EKF method provides a first-order approximation of the optimal solution, since the state distribution is approximated by a Gaussian that is propagated at each step using the first-order linearization of the non-linear dynamics ($x_{k+1} \approx A_k x_k + B_k v_k$). This approximation introduces large errors in certain cases \cite{wan2000unscented}.}, and in most cases replaced by the UKF (Unscented Kalman Filter), it has proven to be accurate enough for this implementation. Another important fact to be taken into account is that the optimality of the method relies on the assumption that the process noises introduced in the model are zero-mean normal distributions, which is not true in this case. However, this assumption impacts mostly on the time it takes for the filter to converge to the real parameters.

In accordance with the fluid modeling approach, we assume that the measurements of $q$ and $r$ (integer-valued states) are actually quantized measurements of the real ones (real-valued states). The filtering scheme contemplates this quantization effect as zero-mean measurement noise in both states, where its standard deviation was computed from the experiments performed over the real system. The reason behind this decision is that, in practice, simpler recursive parameter estimation algorithms showed quite oscillatory results due to the integer-valued nature of the state variables. Figure \ref{estimation_ekf} illustrates the described behavior of a recursive estimation algorithm and the EKF tuned to have similar time responses.
%(such as $\hat{p}_k = \hat{p}_{k-1} + \alpha(r_k-)$)

\begin{figure}[h!]
 \centering
 \includegraphics[scale=1]{images/estimation_ekf_2}
 \caption{Comparison between methods for estimating the parameter $p$: a recursive estimation algorithm (with form $\hat{p}_k = \hat{p}_{k-1} + \alpha_k e$, being $e$ the prediction error computed from $\hat{p}_{k-1}$ and $\alpha_k$ the updating gain) and the EKF were tried and compared in simulation.}
 \label{estimation_ekf}
\end{figure}

\begin{figure}[h!]
 \centering
 \includegraphics[scale=1]{images/estimation_ekf}
 \caption{Comparison between methods for estimating the parameter $p$: a recursive estimation algorithm (with form $\hat{p}_k = \hat{p}_{k-1} + \alpha_k e$, being $e$ the prediction error computed from $\hat{p}_{k-1}$ and $\alpha_k$ the updating gain) and the EKF were tried and compared in simulation.}
 \label{estimation_ekf}
\end{figure}

%Then, the propagation of the covariance $P$ is determined by the first order linearization of this dynamics,
%\begin{align}
%	P_{k|k-1} &= A_k P_{k-1|k-1}A_k^T + Q_k \\
%    A_k &= \begin{bmatrix}
%    1 & 0 & 0 & -1 & 0 & 0\\
%    0 & 1-p_k \Delta t & 0 & 1 & -r_k \Delta t & 0\\
%    0 & 0 & 1-\alpha \Delta t & k_{in} \beta (b_{k-i})^{\beta-1} & 0 & (b_{k-2})^\beta \Delta t\\
%    0 & 0 & 1 & 0 & 0 & 0\\
%    0 & 0 & 0 & 1 & 0 & 0\\
%    0 & 0 & 0 & 0 & 0 & 1
%    \end{bmatrix}
%\end{align}

The selection of the initial parameters can also be relevant to the performance of the closed loop. As the main objective of the implementation is to avoid overloading, we initialized the filter by "overestimating" the impact of the jobs onto the fileserver, as well as their time duration. This basically means to start with a very high initial estimation $\hat{k}^1_{in}$ and $\hat{k}^2_{in}$ so that, during the transient of the EKF, the prediction of the fileservers' loads remains always above the real value. Likewise, a very low initial value of $\hat{p}$ leads to a predictive model that processes jobs at a slower rate than the real one, which leads to a lower, more conservative, submission rate. With regard to the state variables, there is no need of such decision since, at the beginning of every execution, $q$, $r$ and $b$ are known to be equal to $0$.

\section{Control scheme}
For the control loop, we implemented a model-predictive controller, mainly for the advantages that such an approach can deliver:
\begin{itemize}
\item It can easily deal with the constraints (e.g. the control input $u$ being positive and integer) and non-linearities (e.g. the non-linear impact of $b$ on the fileserver load) of the model.
\item The model used for prediction is updated at each step with the parameters estimated by the EKF, making the scheme inherently \emph{adaptive} to this variations in the model.
\item It can cope with multiple objectives, as desired in this work.
\end{itemize}

\begin{figure}[h!]
 \centering
 \includegraphics[scale=1.2]{images/control_scheme}
 \caption{Control scheme}
 \label{control_scheme}
\end{figure}

Figure \ref{control_scheme} illustrates the structure of the controller. The optimization process was simulated in Python using the constrained non-linear optimization library from SciPy, and then implemented in Ruby with the Ruby/GSL library for numerical computing. 

For solving the optimization problem with positive integer constrained $u$, the algorithm first solves the "relaxation" of the problem, where the input is indeed real-valued, and then it rounds (up or down) the solution to achieve an admissible control input. In further experiments, we also tried an exhaustive optimization method. This was possible given that the set of admissible control values is limited (and heavily reduced) by the positive integer-valued constraint on the input, which allows to compute all possible future values in terms of all possible values of $u$, and then keep the optimal one. At the end, this method turned out to be faster in terms of computation times, and required no further libraries for numerical computation. For these reasons, the final version of the algorithm was developed using the latter.

As for the model-predictive scheme, a constant parametrization in the input was enforced, to simplify the optimization problem. Additionally, the prediction horizon was fixed to $N=3$.

During the experiments, two different objective functions were formulated and used, depending on the desired goal:

\begin{itemize}
	\item \textbf{Maximize cluster usage}: as seen in previous chapters, maximizing the cluster's usage can be accomplished by guaranteeing enough waiting jobs $q$ at every instant. Selecting a high value of $q_{des}$ (around 30 or 40) has shown satisfying results in this regard,
	$$
	J^1_k(u) = \sum_{j=0}^{N} \big(q_{k+j | k} - q_{des}\big)^2.
    $$
    
    An alternative would be to minimize $(r_{k+j|k} - r_{max})^2$, which might seem the most obvious solution. However, the only way to estimate $r_{max}$ is to inject more jobs into the cluster, which has to be done by increasing the waiting queue $q_k$ until no more jobs are being allocated. As a result, the only way to maximize $r_k$ is to guarantee that $q_k>0$ in steady-state.
    
    \item \textbf{Avoid fileserver overloading}: from the computer science point of view, a system is overloaded if the \emph{loadavg} is higher than the number of cores in the system (a rule of thumb widely used by system's administrators). Under this criteria, the value of the reference is dynamically adjusted to match the number of cores in the fileserver,
    $$
    J^2_k(u) = \sum_{l=1}^{M} \sum_{j=0}^{N} \big(f^l_{k+j | k} - f^l_{des}\big)^2
    $$
\end{itemize}

where $M$ is the number of fileservers in the infrastructure, and $f^l_{des}$ the number of cores in the fileserver $l$.