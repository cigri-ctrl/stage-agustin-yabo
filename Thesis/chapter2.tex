\setcounter{equation}{0}
\chapter{Introduction}

\section{Motivation}

\subsection{Autonomic administration}

HPC (High-Performance Computing) systems have increasingly become more varying in their behavior, in particular in aspects such as performance and power consumption, and the fact that they are are becoming less predictable demands more runtime management, which usually requires human intervention.
Some of the causes behind this change are:
\begin{itemize}
\item uncertainties concerning actual runtime performances (e.g. execution times, load impact on the infrastructure) compared to evaluations which were used for off-line scheduling.
\item variations in data access times (due to e.g. more and more elaborate cache mechanisms).
\item variations related to the values in the data (e.g. number of iterations or depth of computation depending on variables’ precision).
\end{itemize}
Coping with such phenomena must be done at runtime, based upon measurements performed during execution time. This can be done in response to monitored sensors of the systems, by analysis of this data and utilization of the results in order to trigger
appropriate system-level or application-level feedback mechanisms. Such mechanisms rely on extensive monitoring and analysis, and involve decisions making and execution. These feedback loops, in the domain of Computer Science, are the object of Autonomic
Computing \cite{kephart2003vision}, which emerged mainly from distributed and Cloud systems at IBM.

\subsection{Control theory in High Performance Computing}

One approach in designing feedback loops is Control Theory, extensively widespread in all domains of engineering, but only quite recently and scarcely applied to regulation in computing systems \cite{ctrl-theory-comp, sefsas-fb-assurances}. It can bring to systems designers methodologies to conceive and implement feedback loops with well-mastered and guaranteed behavior in order to obtain automated
management with goals of optimization of resources or avoidance of crashes and overloads.

Different control designs have been proposed in the past, aiming to solve different classes of control problems and involving different dynamical representations of the systems. Classical control addresses quantitative dynamics, using continuous models based on differential equations. Discrete Event Systems concern systems where the dynamics involves a set of discrete, logical states (possibly finite), and events causing transitions: they are commonly modeled using Petri nets or finite state automata. Probabilistic aspects are also addressed with Markov Decision Processes. The control problems encountered in complex computing systems can be of various kinds, and represent a new application domain for Control Theory, for which research is still identifying and evaluating
modeling approaches.

\section{State of the art}

\subsection{Control-based scheduling}

The scheduling problem in computing systems is one of the first problems to be addressed in the past by mean of control theory. As one of the pioneering works, Lu \cite{lu2002feedback} first introduces the idea of applying control theory to real-time scheduling (FCS) in \emph{unpredictable} environments, where systems workloads cannot be accurately modeled. In this paper, the author aims to control: the miss ratio (number of deadline misses / total number of tasks) and CPU utilization, both measured in a time window, by affecting the cluster's \emph{total estimated utilization}. %The selection of this control input is due to the fact that the total estimated utilization adjusts the QoS variable of a EDF scheduling algorithm that acts as an admission controller for the task queue. The author models each task $T_i$ with $N$ QoS levels, where each QoS level is characterized by the relative deadline $D$, the estimated execution time $EE$ and the actual execution time (unknown for the scheduler), and differentiates between periodic and aperiodic tasks. \\

In later works \cite{lu2005feedback}, the author states that previous approaches were based in the assumption that tasks on different processors are independent from each other, and cannot be applied in distributed systems that uses end-to-end task model (e.g. distributed real-time embedded systems). He proposes a scheme where each task $T_i$ is a chain of $m$ subtasks $T_{i1}, ..., T_{im}$ that run on $n$ different processors, and so each CPU cannot be controlled independently since the performance is coupled among them (representing this coupling by a $n\times m$ subtask allocation matrix $F$). At the same time, each subtask has an associated estimated execution time that can vary at runtime. For tackling this problem, he proposes a MIMO controller to control each CPU's utilization.

\subsection{High Performance Computing}

A predictable HPC system was designed in \cite{park2011predictable} through feedback and admission control. In this work, \citeauthor{park2011predictable} control each job's waiting time and running time using virtual machine abstractions, by running jobs inside performance containers and regulating the access of this containers to real system resources. In this way, by means of control theory, they either \emph{accelerate} or \emph{slow down} each job so that it meets the required deadlines, achieving predictable scheduling. Moreover, they perform admission control by deciding to accept or reject incoming jobs in terms of their deadlines.
To achieve this, they model each job's progress with a first order linear difference equation. However, the proposed solution assumes each job's progress can be measured by the algorithm, which is not always true in HPC environments.

In \cite{ghazzawi2012mpc}, a model predictive admission control approach is proposed for performing real-time multi-objective scheduling in multi-CPU clusters. They argue that MPC controllers can tackle the presence of multiple optimization objectives, unlike PID controllers, but requires more empirical investigation. The latter is further developed in \cite{ghazzawi2012control}, where they model and identify the scheduler behaviour and the HPC cluster of an industrial workflow management system, and they later use this model for an MPC approach. The general objective is to control slack and CPU utilization through admission control. In their experiments, they divided jobs into 2 categories (called \emph{Data} and \emph{Design}), with different time duration each, and they worked under the assumption that the release rate of tasks cannot be controlled (in contrast with \citeauthor{park2011predictable}'s work). Nevertheless, in this approach, they assume to count on \emph{a priori} knowledge of the job's duration (that should be provided by the user), leading to a control scheme that is not robust to deviations from the analyzed workflows.

\subsection{General panorama}

Control theory has been scarcely applied in High-performance Computing. Previous approaches in the scene propose rather simple linear models that, to our best understanding, lack the ability to reflect several non-linear behaviors and variable constraints of the real system, since their accuracy is limited to a specific \emph{region of operation}. Thus, the improvements are limited to this region, and the controller fails to ensure the desired performance and stability objectives for all possible situations.

Moreover, this work is intended to investigate a branch of HPC that has not received much (if any) attention from the control community: scientific workflows management. In this particular problem, there is a series of assumptions that can greatly simplify the general HPC scheduling problem (e.g. specific probability distributions for execution times), without having to deal with large comprehensive dynamical models that contemplates all possible situations. In addition, the proposed solution do not seek to replace the scheduler of the infrastructure but to complement it, ensuring robustness to variations in the analyzed scenarios.

\section{Scientific contributions}

This work draws from a previous one where the main contribution was a proof of concept \cite{stahl2018}. We showed, for this particular case study, the benefits of re-thinking the computing infrastructure as a dynamical system, to allow the use of the control theory toolset in order to improve performance measures of the grid. In this latter, the cluster dynamics were modeled as a second order system, considering its controlled variable as the number of jobs in the cluster's waiting queue. Then, a proportional control loop was implemented in order to track a reference number of jobs by regulating the amount of jobs submitted to the cluster. We showed an improvement in cluster's usage, as well as a reduction of the total computation time of a set of sample scientific workflows.

While mainly focusing on avoiding cluster overload, our previous work didn't consider other problems such as filesystem overload. Filesystem usage depends directly on the amount of running jobs in the cluster, and thus controlling the amount of waiting jobs is not enough for this task. In this work, we present a model that comprehends both the waiting queue as well as the running jobs in the cluster, based on fluid modeling theory \cite{malrait2011experience}. Along with this model, we investigate the dynamics of a fileserver's load average, and propose a scheme to measure the impact of scientific workflows onto it. Later on, we implement a model-predictive control loop that relies on an EKF (Extended Kalman Filter) for online parameter estimation to provide an accurate prediction. Ultimately, the scheme contemplates two objectives: a) maximize cluster usage, and b) regulate the fileserver's load.