\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Abstract}{ii}{figure.caption.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Autonomic administration}{1}{subsection.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.2}Control theory in High Performance Computing}{1}{subsection.1.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}State of the art}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Control-based scheduling}{2}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}High Performance Computing}{2}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.3}General panorama}{3}{subsection.1.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Scientific contributions}{3}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}System description}{4}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Architecture}{4}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}CIMENT center}{4}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}CiGri middleware}{4}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{A lightweight grid}{4}{subsubsection*.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bag-of-tasks}{5}{subsubsection*.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Utilization policy}{5}{subsection.2.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Best-effort jobs}{6}{subsubsection*.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Problems considered}{7}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Resources under-utilization}{7}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Fileserver overload}{7}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}Cluster overload}{8}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Platform and methods}{8}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Modeling and control}{9}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Queue modeling}{9}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Previous work}{9}{subsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Components involved}{10}{subsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Overview}{10}{subsubsection*.11}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Maximum number of jobs allowed $r_{max}$}{10}{subsubsection*.14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Job allocation's buffer $b$}{11}{subsubsection*.15}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Spilling rate}{11}{subsubsection*.16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}Fluid modeling}{12}{subsection.3.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dynamical model}{12}{subsubsection*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Maximum cluster usage}{12}{subsubsection*.18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Simulation model}{13}{subsubsection*.19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.4}Model validation}{13}{subsection.3.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Filesystem analysis}{15}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Considered scenario}{15}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Measuring disk I/O}{15}{subsubsection*.22}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Dynamical model}{15}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Data reading}{16}{subsubsection*.24}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Model validation}{17}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}Multi-storage infrastructure}{17}{subsection.3.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Parameter estimation}{19}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Control scheme}{21}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Results}{23}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Maximizing cluster usage}{23}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Controlling fileserver usage}{25}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Discussion}{28}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Conclusion}{28}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Perspectives}{28}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Bibliography}{29}{section.5.2}
