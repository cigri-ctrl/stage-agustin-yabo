\chapter{System description}

\section{Architecture}

\subsection{CIMENT center}

CIMENT center is one of the most powerful High Performance Computing (HPC) tier-2 centers in France. Located in Grenoble, it is a joint center from the Community Universit\'e Grenoble Alpes and more than 30 research laboratories from CEA, INRIA and CNRS. It is composed of over 6500 cores that add up to 135 TFlop/s and 28 TB of memory distributed over 12 clusters.

The center provides HPC resources to academic research communities from a wide range of disciplines: Biology and Health,
Chemistry, Environment and Climate, Numerical Physics,
Earth and Planetary Sciences, and Distributed Computing
\cite{biscarat2014synergy}. As an example, the center has been extensively used in the ATLAS particle detector experiment \cite{aad2008atlas}, one of the seven experiments performed at the LHC (Large Hadron Collider) particle accelerator, and one of the two involved in the discovery of the Higgs boson.

\subsection{CiGri middleware}

\subsubsection{A lightweight grid}

CiGri\footnote{http://ciment.ujf-grenoble.fr/CiGri/dokuwiki/doku.php} is a simple, lightweight, scalable and
fault tolerant grid system designed to exploit the unused resources of a set of computing clusters. The software was developed at Universit\'e Grenoble Alpes and actively used on the production clusters of the CIMENT center. The CiGri server software is a modular application based on a database, that provides the users with an interface for executing large scale scientific computations. It interacts with the computing clusters through OAR \cite{capit2005batch}, a modular batch scheduler for HPC clusters. This lightweight grid system is a simplified version of the general grid concept \cite{foster2003grid}, in particular a certain homogeneity of services and administration procedures are adopted. The platform supports only one specific form of job submission: the bag-of-tasks workload, composed of a set of independent parametric jobs. For this approach, this sequential jobs represent the lowest level of granularity to be handled.

\subsubsection{Bag-of-tasks}
The nature of this specific kind of workload arises from the need of running large-scale scientific computations with different parameters, by means of grid computing infrastructures. An example of this implementation is MCFOST \cite{pinte2006monte}, a 3D radiative transfer code based on Monte-Carlo method, which can usually amount to 30000 tasks with average computation times of 45 minutes per job. The fact that jobs are launched with different parameters leads to non-trivial distributions of execution times, as can be seen in sample histograms shown in Figure \ref{jobs_duration}. Still, they show small variability in their execution time: according to our analysis of historical data from CiGri database, distribution of computation times exhibits coefficients of variability \cite{bendel1989comparison} that ranges from 10\% to 30\%, indicating minor dispersion. 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.8\columnwidth]{images/no_bell}
\end{center}
\caption{Campaign histograms for different workloads handled by CiGri.}
\label{jobs_duration}
\end{figure} 

\subsection{Utilization policy}

The computing grid supports campaign launching through two different submission mechanisms:
\begin{itemize}
	\item Direct job submission through OAR: the user decides the cluster on which the job is going to run, and submits it through its \emph{Resource Job Management System} to the \emph{Local users} queue in the cluster (Figure \ref{fig:sysarchi}).
	\item Campaign submission through CiGri: the user submits a campaign, which yields a parametric bag-of-tasks in the CiGri server (Figure \ref{fig:sysarchi}), and the \emph{Runner} module subsequently sends its jobs to a dedicated waiting queue in each OAR cluster. This way, CiGri balances the amount of jobs submitted onto all the clusters, according to certain criteria to be described in succeeding chapters. 
\end{itemize}

\begin{figure}[!h]
\centering
\includegraphics[width=0.7\columnwidth]{images/cigri_oar_irods}
%%%%\fbox{\ldots}
%%%\\[10ex]
\caption{The system global architecture.}
\label{fig:sysarchi}
\end{figure}

Once the jobs reach the cluster, the OAR scheduler will eventually submit them to the computing nodes. This submission policy is based on several factors such as job priority, expected time duration, dependencies, resources availability, etc. managed specifically by the scheduling algorithm.
Throughout the execution, jobs may or may not require input data to start (e.g. file reading from the storage), as well as output data to be stored (e.g. file saving to the storage). The downloading and uploading of this data is handled by \emph{iRODS}, a distributed storage management system, also illustrated in Figure \ref{fig:sysarchi}.

\subsubsection{Best-effort jobs}

In order to exploit idle resources in a transparent manner, the solution introduces the concept of \emph{best-effort} jobs into OAR. The key aspect of best-effort jobs is that they are treated as 0-priority tasks by the scheduler, by assigning them into a dedicated best-effort queue in the cluster. This means that, if during their execution in a specific resource, this least is requested by a local cluster user, the job is killed by the local batch scheduler and later resubmitted according to specific fault-treatment mechanisms \cite{georgiou2007evaluations}. Hence, the challenge of CiGri is to guarantee the complete execution of this scientific computations in spite of resources' volatility, in the most efficient way. This implies maximizing the usage of idle resources by best-effort jobs, without overloading any component of the infrastructure.

Noteworthy, the main difference with other scheduling problems is that walltime is not a hard constraint when running best-effort jobs. The computation time of a scientific workload is strictly limited by the resource availability in the grid, which is determined by users usage. For this, and as previously stated, meeting deadlines is not within the scope of CiGri.

\section{Problems considered}

The main objective of this project is to improve the job submission mechanism implemented in CiGri. To our understanding, the way CiGri handles job submission to the cluster queue is a major factor in the overall performance and, thus, maximizing the exploitation of idle resources requires an improved strategy in this regard. The current {\em ad-hoc} solution is based on a \emph{tap} mechanism implemented in the \emph{Runner} component of CiGri that maps jobs from the bag-of-tasks on the cluster queue with the following criteria,

\begin{pseudocode}{Job submission}{\,}
\label{Pseudocode}
\MAIN
rate \GETS 3 \\
increase\,factor \GETS 1.5 \\
\WHILE \text{jobs left in the bag-of-tasks} \DO 
\BEGIN
	\IF \text{jobs running} = 0
	\THEN
	\BEGIN
		\text{launch } rate \text{ amount of jobs} \\
		rate \GETS \min(rate * increase\,factor,100)
	\END \\
	\WHILE \text{jobs running} > 0 \DO \text{sleep until timeout}
\END
\ENDMAIN
\end{pseudocode}

\subsection{Resources under-utilization}

The algorithm cyclically submits $rate$ number of jobs to the cluster, which increases at every iteration. Note that, in each cycle, the algorithm waits for the completion of all submitted jobs, but under several failsafe mechanisms intended to protect the infrastructure. For example, the loop timeouts when a job's walltime is exceeded. Other more complex scenarios are also considered by the algorithm, which might involve cluster \emph{blacklisting} depending on the severity of the problem, requiring administrator intervention to regularize the situation.

Under the explained behavior, jobs are submitted onto a cluster \textbf{only} when its waiting queue is fully empty. In practice, this behavior yields situations where the cluster is being under-used (or not used at all) in spite of the existence of remaining jobs in the bag-of-tasks.

\subsection{Fileserver overload}

Every scientific job consumes storage resources, whether for reading input files from a server, or for storing the output of the script. The simplest I/O task can greatly affect a fileserver's performance when running hundreds of simultaneous jobs. This makes storage a major challenge in parallel computing infrastructures, and a limiting factor in the performance of the grid \cite{jain2012input}. In particular, in the CiGri infrastructure, it is specially difficult to identify the source of the overloading when running multiple campaigns simultaneously. And even when running a campaign that is known to have heavy I/O operations, the server (or servers) on which the operations are going to be performed is unknown in advance. All these issues are currently being tackled manually, requiring an operator to intervene when needed.

\subsection{Cluster overload}

Currently, the system includes a failsafe mechanism that avoids submitting jobs to a cluster that is \emph{stressed}. In this regard, the definition of stressed is up to the criteria of the system administrator, as there are several sensors that could indicate an overloaded server (CPU load, memory usage, active processes, etc.). By default, CiGri queries the average CPU load of the server and checks whether it is above or below a certain threshold value ($0.8$ by default).

\section{Platform and methods}
\label{platform}

During the project, several experiments were performed in order to identify and validate the proposed models. Every experiment was first executed onto a local Docker\footnote{https://www.docker.com/} environment, and then on the Grid5000 project. In every case, the environment was composed of a CiGri server, an OAR scheduler, and a cluster with a specific number of resources (depending on the experiment). In the storage analysis, additional servers were deployed to use as fileservers.

The workflows used for experimentation were composed of dummy jobs ($sleep$ Linux command) with random duration, based on the distributions analyzed in Figure \ref{jobs_duration}. The impact on the fileserver load was simulated with the \emph{dd} command\footnote{A command-line utility for Unix-based operating systems used for file copying and conversion.} with \emph{/dev/null} as input. This way, we managed to reduce the stress coming from network usage by generating the data locally (as network overloading was not within the scope of the project).

In some cases, we stressed the system with external disturbances by submitting higher priority jobs to the cluster, in order to dynamically vary the amount of available resources.

Numerical calculations as well as dynamical simulations (for validation and control) were first tested in Python, and then implemented in Ruby for testing along CiGri.


