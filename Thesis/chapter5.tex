\chapter{Results}

While every experiment was first simulated in Python, as described in \ref{platform}, the results shown in this section were obtained from experiments performed in a real environment deployed on the Grid5000 project. The procedure implies,
\begin{enumerate} 
\item Setting up a reduced infrastructure composed of up to 5 servers.
\item Implementing the CiGri system, programmed in Ruby, and modified according to the methods proposed.
\item Launching campaigns of several jobs.
\item Retrieving the results in a plain text format.
\item Parsing the results from text in Python for post-processing (e.g. interpolating the measurements to the same time vector) and further analysis and plotting.
\end{enumerate}
Moreover, given the significant amount of time required to process each campaign (and thus, to perform each experiment), we implemented an online algorithm for displaying in real-time the evolution of the system state, by querying iteratively the logs of the CiGri server through SCP (a command-line utility for file transfer through the SSH protocol).

\section{Maximizing cluster usage}

We performed 10 similar experiments in a cluster composed of 12 resources, in which we compared results when applying the naif approach, a PI controller and our MPC controller onto the waiting queue $q$ when launching a 400 jobs campaign. While the advantages of implementing a PI controller (Figure \ref{pi_results}) instead of the original method (Figure \ref{naif_results}) were already stated in previous work \cite{stahl2018}, there were no significant improvements when using the MPC approach. This is coherent with the conclusions obtained in \ref{maximum_usage_section}: guaranteeing a minimum bound on the amount of waiting jobs will lead to cluster maximization, as the scheduler will allocate as many jobs as possible. In every case, the average cluster usage was computed as,
\begin{align}
	Usage [\%] = \frac{1}{t_f-t_0} \sum_{k=t_0}^{t_f} \frac{r_k \Delta t}{r_{max}}, \nonumber
\end{align}
being $t_0$ and $t_f$ the initial and final time of the campaign execution respectively.

\begin{figure}[!h]
\begin{center}
\includegraphics{images/naif_results}
\end{center}
\caption{Open loop behavior (i.e. original job submission policy). The algorithm yields an average cluster utilization of $77\%$.}
\label{naif_results}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics{images/pi_results}
\end{center}
\caption{Closed loop behavior with the PI controller. The closed loop system yields an average cluster utilization of $85\%$.}
\label{pi_results}
\end{figure}

The same results can be seen when simulating the effect of external users by varying the amount of available resources in the cluster, as a more realistic scenario (Figure \ref{disturbance}). The number of available resources were varied randomly by occupying resources in a range of 0-12 equiprobably, making the expectancy of resource availability equal to 6 (half of the total resources), so the completion time of the campaign is expected to be twice as much as that of non-disturbed experiments. While the reference tracking in the waiting queue is affected by this disturbances, cluster usage is still maximized, and so the MPC scheme shows no improvement in this regard.

\begin{figure}[!h]
\begin{center}
\includegraphics{images/disturbance_long}
\end{center}
\caption{Closed loop behaviour with the PI controller subject to variations in the available resources.}
\label{disturbance}
\end{figure}

For every experiment, the reference of waiting jobs was set to 40 as it proved to be a value that achieved maximum cluster usage without producing significant stress on the scheduler.

\section{Controlling fileserver usage}

In order to test the fileserver usage strategy, we performed a number of experiments on an infrastructure composed of a 60 resources cluster, and 2 fileservers with 4 cores each (which, as previously expressed, are assumed to be overloaded when $f_k>4$). Then, we applied the cluster usage maximization strategy when computing a test workflow with average duration of 60 seconds. Each job in the workflow reads a file of 2 mb from the first fileserver, while the second fileserver is left unused. The results are as expected: there is a major impact on the fileserver load, that surpasses the value of 30 (Figure \ref{max_usage}).

\begin{figure}[!h]
\begin{center}
\includegraphics{images/results_max}
\end{center}
\caption{Maximizing the used resources $r_k$ yields an overload in the first fileserver (with load $f^1_k$) while it doesn't affect the second fileserver (with load $f^2_k$).}
\label{max_usage}
\end{figure}

We subsequently implemented the control loop for avoiding fileserver overload, that contemplates both loads $f^1_k$ and $f^2_k$. The results of the experiment can be seen in Figure \ref{fs_limit}, which shows how the controller is able to keep the load $f^1_k$ under the desired value. As a result, it reduces considerably the cluster usage to around $45\%$ (computed from the average of all experiments) and, conversely, increases the total computation time of the campaign from $250\,s$ in the first experiment to $345\,s$.

It is noteworthy that reducing the average cluster usage in around $50\%$ achieves a reduction in the peak of the fileserver's load of $9$ times between experiments (from $36.71$ in the first experiment to $4$ in the second one).

\begin{figure}[!h]
\begin{center}
\includegraphics{images/results_fs}
\end{center}
\caption{Results of controlling the load of both fileservers $f^1_k$ and $f^2_k$.}
\label{fs_limit}
\end{figure}

Moreover, the EKF is able to estimate the processing rate $p$, and the gains $k^1_{in}$ and $k^2_{in}$, that reflects the impact of the workflow onto the fileservers. Given that the second fileserver is not used by the campaign, the associated gain $k^2_{in}$ remains null throughout the execution.

\begin{figure}[!h]
\begin{center}
\includegraphics{images/estimation}
\end{center}
\caption{Results for online parameter estimation of $\hat{p}$, $\hat{k}^1_{in}$ and $\hat{k}^2_{in}$ using the EKF.}
\label{parameter_estimation}
\end{figure}
