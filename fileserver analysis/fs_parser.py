# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import datetime
import matplotlib.pyplot as plt
from collections import defaultdict
from scipy import interpolate
from scipy import signal

jobs = defaultdict(lambda: [[],[],[],[]]) # Start, Stop, Temp, Vector
fss = defaultdict(lambda: [[],[]]) # Temp, Value

def date2unix(date):
    return time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S+01").timetuple())

strLogFolder = '.'

# The time range I am interested in
time_range = [0,1000000]
#time_range = [0,130000]
#time_range = [410000,450000]
#time_range = [575000,615000]

with open('output.csv', "r") as csvfile:
    
    legends = []
    csvarray = csv.reader(csvfile, delimiter=',', quotechar='|')
    i = 0
    plt.figure(0); plt.xlim(time_range)
    
    for row in csvarray:
        # Store the initial time to bias the axis
        if i == 0:
            initial_t = date2unix(row[9])
            i = 1
        
        # Dont look at the hole interval
        if date2unix(row[9])-initial_t > time_range[0] and \
           date2unix(row[9])-initial_t < time_range[1]:
            jobs[row[1]][0].append(date2unix(row[9])-initial_t)
            jobs[row[1]][1].append(date2unix(row[10])-initial_t)
        
    
    for campaign in jobs:  
        job_vect = np.hstack([np.ones(len(jobs[campaign][0])),
                           -np.ones(len(jobs[campaign][1]))])
        
        job_tmp = np.hstack([jobs[campaign][0],jobs[campaign][1]])
        
        zips = sorted(zip(job_tmp,job_vect))
        
        jobs[campaign][2] = [y for y,x in zips]
        jobs[campaign][3] = np.cumsum([x for y,x in zips])
        
        plt.plot(jobs[campaign][2],jobs[campaign][3])
        
        legends.append(campaign)
        
    plt.legend(legends)    
    csvfile.close()
    
with open('mantis_loads.csv', "r") as csvfile:
    
    legends = []
    csvarray = csv.reader(csvfile, delimiter=':', quotechar='|')
    i = 0
    plt.figure(1); plt.xlim(time_range)
    
    for row in csvarray:
        
        # Dont look at the hole interval
        if np.double(row[1])-initial_t > time_range[0] and \
           np.double(row[1])-initial_t < time_range[1]:
            fss[row[0]][0].append(np.double(row[1])-initial_t)
            fss[row[0]][1].append(np.double(row[2].split(' ')[0]))
        
    for fs in fss:  
        plt.plot(fss[fs][0],fss[fs][1])
        legends.append(fs)
        
    plt.legend(legends)    
    csvfile.close()

############################################################   
# Analysis 1 between Quath and 11283 in [575000,615000]
############################################################

x = fss['quath']
y = jobs['11283']

f = interpolate.interp1d(x[0], x[1])
x[1] = f(y[2])

plt.figure(2)
plt.title('Quath vs 11283')
plt.plot(y[2],x[1])
plt.plot(y[2],y[3])

plt.figure(3)
plt.title('Coherence between 11283 and Quath')
f, Cxy = signal.coherence(y[3],x[1])
plt.semilogy(f,Cxy)
plt.xlabel('frequency [Hz]')
plt.ylabel('Coherence')



